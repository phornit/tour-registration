<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Auth
Route::post('authenticate', 'Auth\LoginController@authenticate');
Route::get('logout', 'Auth\UserProfileController@logout');
Route::post('register' , 'Auth\RegisterController@UserRegister');
Route::put('changePassword', 'Auth\UserProfileController@changePassword');
Route::post('createUserProfile/{sec_id}', 'Auth\UserProfileController@createUserProfile');
Route::put('updateUserProfile/{id}' , 'Auth\UserProfileController@updateUserProfile');
Route::get('getCurrentUser' , 'Auth\UserProfileController@getCurrentUser');
Route::get('audits', 'Auth\AuditController@index');

//Menu
Route::get('getAllBackendMenu', 'Backend\MainMenuController@getAllBackendMenu');
Route::get('getAllParentBackendMenu', 'Backend\MainMenuController@getAllParentBackendMenu');
Route::put('update-ordering', 'Backend\MainMenuController@updateOrdering');

Route::put('trashed/{id}', 'Backend\MenuController@trashed');
Route::get('getParentMenu', 'Backend\MenuController@getParentMenu');
Route::get('getSubParentMenu/{id}', 'Backend\MenuController@getSubParentMenu');

//Backend
Route::get('getPackageByCountry/{id}', 'Backend\TourAgencyController@getPackageByCountry');
Route::get('getAllTourAgency', 'Backend\TourAgencyController@getAllTourAgency');
Route::get('getAllTourPackage', 'Backend\TourPackageController@getAllTourPackage');
Route::get('getTourPackageRegister', 'Frontend\TourTrackingController@getTourPackageRegister');

Route::put('approveTourPackage/{id}', 'Backend\TourPackageController@Approve');
Route::put('disapproveTourPackage/{id}', 'Backend\TourPackageController@DisApprove');

Route::get('summaryTourPackage', 'Backend\ReportController@summaryTourPackage');
Route::get('tourPackageReport', 'Backend\ReportController@tourPackageReport');
Route::get('summaryTourPackageByCountry', 'Backend\ReportController@summaryTourPackageByCountry');

Route::get('getPackageByAgency/{id}', 'Backend\TourPackageController@getPackageByAgency');

Route::get('searchTouristsBackend/{id}', 'Backend\TouristsController@searchTouristsBackend');
Route::get('findTourists', 'Backend\TouristsController@findTourists');
Route::get('getTouristsBackend/{id}', 'Backend\TouristsController@getTouristsBackend');
Route::get('get_tourists_by_package_id/{id}', 'Backend\TouristsController@get_tourists_by_package_id');

//Statistic
Route::get('uniqueVisitor', 'Auth\StatisticController@uniqueVisitor');

Route::get('getAgency', 'Frontend\TourAgencyController@getAgency');

Route::put('subject/trash/{id}', 'Backend\SubjectController@trash');
Route::get('getAllSubjects', 'Backend\SubjectController@getAllSubjects');

Route::get('findUser', 'Auth\UserController@search');
Route::put('approve/{id}', 'Auth\UserController@Approve');
Route::put('disapprove/{id}', 'Auth\UserController@DisApprove');
Route::put('mot_approve/{id}', 'Auth\UserController@MotApprove');
Route::put('mot_disapprove/{id}', 'Auth\UserController@MotDisApprove');


Route::get('findAgency', 'Backend\TourAgencyController@findAgency');
Route::get('findPackage', 'Backend\TourPackageController@findPackage');

Route::put('trash/{id}', 'Backend\ContentController@trash');

//CoreBackEnd
Route::post('fileUpload', 'Backend\FileUploadController@fileUpload');
Route::post('multipleFileUpload', 'Backend\FileUploadController@MultipleFileUpload');
Route::post('deleteImage', 'Backend\FileUploadController@deleteImage');
Route::post('removeImage', 'Backend\FileUploadController@removeImage');
Route::post('deleteFile', 'Backend\FileUploadController@removeFile');
Route::post('resizeImagePost', 'Backend\FileUploadController@resizeImagePost');
Route::post('imagePost', 'Backend\FileUploadController@imagePost');


$arr_backend =	[
    //Auth
    'users' => 'Auth\UserController',
    'role' => 'Auth\RoleController',
    'userProfile' => 'Auth\UserProfileController',

    //Backend
    'backendMenu'	=> 'Backend\MainMenuController',
    'menus' => 'Backend\MenuController',
    'resources' => 'Backend\ResourceController',
    'tourAgencyBackend' => 'Backend\TourAgencyController',
    'tourPackageBackend' => 'Backend\TourPackageController',
    'touristsBackend' => 'Backend\TouristsController',
    'country' => 'Backend\CountryController',
    'city' => 'Backend\CityController',
    'backendtourAgency' => 'Backend\TourAgencyController',
    
    //Statistic
    'statistic' => 'Auth\StatisticController',

    //modules
    'modules' => 'Backend\ModuleController',

    //Frondend
    'tourAgency' => 'Frontend\TourAgencyController',
    'tourists' => 'Frontend\TouristsController',
    'teamLeader' => 'Frontend\TeamleaderController',
    'tourPackage' => 'Frontend\TourPackageController',
    'tourTracking' => 'Frontend\TourTrackingController',
    'itineraryDay' => 'Frontend\ItineraryDayController'
];

foreach ($arr_backend as $key => $value)
{
    Route::apiResources([$key => $value]);
}

Route::put('update-register-data/{id}','Frontend\TourPackageController@update_register_data');
Route::get('get-tour-package-register','Frontend\TourPackageController@getTourPackageRegister');
Route::get('get-tour-package-not-register','Frontend\TourPackageController@getTourPackageNotRegister');
Route::get('get-tour-package-group-by-days/{id}', 'Frontend\TourPackageController@getTourPackageGroupByDays');
Route::get('get-tourists-by-tour-package/{id}', 'Frontend\TouristsController@get_tourists_by_package_id');

// Emums
Route::get('get-tour-package-state', 'EnumsController@TourPakcageStatus');
Route::post('update-tour-package-state/{id}', 'Frontend\TourPackageController@updateTourPackageStatus');


