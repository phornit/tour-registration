<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/verify-account/{id}', 'Auth\RegisterController@verifyAccount');

Route::get('/', 'AdminController@auth');
Route::get('/register', 'AdminController@auth');

Route::get('/backend/{any}', function () {
    return view('admin');
})->where('any', '.*');

Route::get('{any}', function () {
    return view('pages');
})->where('any', '.*');

Route::get('facebook', function () {
    return view('facebook');
});
Route::get('/auth/google', 'Auth\SocialAuthGoogleController@redirectToGoogle');
Route::get('/auth/google/callback', 'Auth\SocialAuthGoogleController@handleGoogleCallback');

Route::get('/auth/facebook', 'Auth\SocialAuthFacebookController@redirectToFacebook');
Route::get('/auth/facebook/callback', 'Auth\SocialAuthFacebookController@handleFacebookCallback');
