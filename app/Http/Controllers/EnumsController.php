<?php

namespace App\Http\Controllers;

use App\Enums\TourPackageStatus;
use Illuminate\Http\Request;

class EnumsController extends Controller
{
    public function TourPakcageStatus()
    {
        $data = TourPackageStatus::ALLSTATE;
        return $this->sendResponse($data);
    }
}
