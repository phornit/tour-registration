<?php

namespace App\Http\Controllers;

use App\Libraries\Backend\StatusResponse;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, StatusResponse;

    public function sendListResponse($data)
    {
        $response = $this->ResponseList($data);
        return $response;
    }

    public function sendResponse($data, $statusCode = 200 , $message = 'Success')
    {
        return $this->Response($data, $statusCode, $message);
    }

    public function sendNotFoundResponse($message = 'Record Not Found')
    {
        return $this->Response(null, 404, $message);
    }

    public function sendInvalidResponse($message = 'Invalid request')
    {
//        return $this->Response(null, 400, $message);

        $response = [
            'data' => null,
            'statusCode' => 400,
            'message' => $message,
        ];

        return response()->json($response);
    }

    public function sendError($error, $errorMessages = [], $code = 404){

        $response = [
            'data' => null,
            'success' => false,
            'statusCode' => 404,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['error_messages'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
}
