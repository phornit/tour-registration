<?php

namespace App\Http\Controllers;


use App\Mail\SendMailable;
use App\Models\Backend\Content;
use App\Models\Backend\Subject;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
//        $navi = FrontEndMenu::build_nav_menu();
//        view()->share('menu', $navi->main_nav);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

    }

    public function mail()
    {
        $name = 'Krunal';
        Mail::to('phornit@gmail.com')->send(new SendMailable($name));
        return 'Email was sent';
    }
}
