<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TourPackageCollection;
use App\Libraries\Backend\CoreFunction;
use App\Mail\TourPackageApproved;
use App\Models\Frontend\Itinerary;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TourPackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');
        $cityId = \Request::get('cityId');

        if (($fromDate == '')||($toDate == '')){
            if($cityId > 0){
                $packageId = Itinerary::where('location_id','LIKE', '%{'.$cityId.'}%')->pluck('package_id');
                $data = TourPackage::whereIn('id', $packageId)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }else{
                $data = TourPackage::orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }
        }else{
            if($cityId > 0){
                $packageId = Itinerary::where('location_id','LIKE', '%{'.$cityId.'}%')->pluck('package_id');
                $data = TourPackage::whereIn('id', $packageId)->whereBetween('arrival_date',[$fromDate, $toDate])->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }else{
                $data = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }
        }
        return new TourPackageCollection($data);
    }

    public function getAllTourPackage(){
        $data = TourPackage::join('tbl_tour_agency','tbl_tour_package.agency_id','tbl_tour_agency.id')
                             ->select('tbl_tour_package.*', 'tbl_tour_agency.name', 'tbl_tour_agency.email')
                             ->orderBy('tbl_tour_agency.id','desc')
                             ->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);                     
    }

    public function getPackageByAgency($agency_id)
    {
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');
        $cityId = \Request::get('cityId');

        if (($fromDate == '')||($toDate == '')){
            if($cityId > 0){
                $packageId = Itinerary::where('location_id','LIKE', '%{'.$cityId.'}%')->pluck('package_id');
                $data = TourPackage::whereIn('id', $packageId)->where('agency_id',$agency_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }else{
                $data = TourPackage::where('agency_id',$agency_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }
        }else{
            if($cityId > 0){
                $packageId = Itinerary::where('location_id','LIKE', '%{'.$cityId.'}%')->pluck('package_id');
                $data = TourPackage::whereIn('id', $packageId)->whereBetween('arrival_date',[$fromDate, $toDate])->where('agency_id',$agency_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }else{
                $data = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->where('agency_id',$agency_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
            }
        }

        return new TourPackageCollection($data);
    }

    public function findPackage(){
        if ($search = \Request::get('q')) {
            $users = TourPackage::where(function($query) use ($search){
                $query->where('title','LIKE',"%$search%");
            })->paginate(20);
        }else{
            $users = TourPackage::orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }

        return new TourPackageCollection($users);
    }

    public function Approve($id){
        $TourPackage = TourPackage::findOrFail($id);
        $TourAgency = TourAgency::where('id', $TourPackage->agency_id)->first();
        $data = $TourPackage->update([
            'is_approved' => 1,
        ]);
        Mail::to($TourAgency->email)->queue(new TourPackageApproved($TourPackage));
        // Mail::to($user->email)->queue(new AgencyApproved($user));
        return $this->sendResponse($data);
    }

    public function DisApprove($id){
        $TourPackage = TourPackage::findOrFail($id);
        $data = $TourPackage->update([
            'is_approved' => 0,
        ]);
        return $this->sendResponse($data);        
    }
}
