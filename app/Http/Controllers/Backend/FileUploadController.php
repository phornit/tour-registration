<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use CKSource\CKFinder\Image;

class FileUploadController extends Controller
{

    public function MultipleFileUpload(Request $request)
    {

        // $destinationPath = $request['originalPart'];
        // $validate = $request['validate'];

        // $this->validate($request, [
        //     'image' => $validate,
        // ]);
        
        $data = [];
        $i = 0;
        foreach($request->file('image') as $file)
        {
            $imageName = time().'.'.$file->getClientOriginalExtension();
            $newImageName = date('Y-m-d-H-i-s').'-'.$i."-".$imageName;
            $file->move('images/contents/Original/', $newImageName);
            $data[$i] = ['name' => $newImageName, 'part' => 'images/contents/Original/'.$newImageName];  
            $i++;
        }

        return $this->sendResponse($data);
        
        // $this->validate($request, [
        //         'image' => $request['validate'],
        // ]);

        // $destinationPath = $request['originalPart'];
        // $arr_file = [];
        // $i = 0;
        // 
        //  {           
        //     foreach($request->file('image') as $file)
        //     {
        //         $name = time().'.'.$file->getClientOriginalExtension();
        //         $file->move($destinationPath.'/', $name);  
        //         $arr_file[$i] = ['name' => $name];  
        //         $arr_file[$i] = ['part' => $destinationPath."/".$name];  
        //         $i++;
        //     }
        //  }
        // return $this->sendResponse($arr_file);
    }

    public function fileUpload(Request $request) {
        $this->validate($request, [
            'myFile' => $request['validate'],
        ]);

        $image = $request->file('myFile');
        $imageName = time().'.'.$image->extension();

        $newImageName = date('Y-m-d-H-i-s')."-".$imageName;
        $destinationPath = $request['part'];

        $image->move($destinationPath, $newImageName);
        $data = [
            'name' => $newImageName,
            'part' => $destinationPath."/". $newImageName,
        ];
        return $this->sendResponse($data);
    }

    public function removeFile(Request $request)
    {
        $part = $request['part'];
        $fileName = $request['filename'];
        $pathFile = $part."/".$fileName;
        if(file_exists(public_path($pathFile))){
            unlink($pathFile);
            $data = ['name' => $fileName];
            return $this->sendResponse($data);
        }
    }


    public function imagePost(Request $request) {
        $thumbnailsPart = $request['thumbnailsPart'];
        $validate = $request['validate'];

        $this->validate($request, [
            'image' => $validate,
        ]);

        $image = $request->file('image');

        $imageName = time().'.'.$image->getClientOriginalExtension();

        $newImageName = date('Y-m-d-H-i-s')."-".$imageName;

        $destinationPath = $thumbnailsPart;

        $image->move($destinationPath, $newImageName);

        $data = [
            'name' => $newImageName,
            'part' => $destinationPath."/". $newImageName,
        ];
        return $this->sendResponse($data);
    }

    public function resizeImagePost(Request $request)
    {
        try {
            $thumbnailsPart = $request['thumbnailsPart'];
            $originalPart = $request['originalPart'];
            $validate = $request['validate'];

            $this->validate($request, [
                'image' => $validate,
            ]);

            $image = $request->file('image');
            $imageName = time().'.'.$image->extension();

            $newImageName = date('Y-m-d-H-i-s')."-".$imageName;

            $img = \Image::make($image->path());
            $img->resize(414, 414, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbnailsPart.'/'.$newImageName);

            $image->move($originalPart, $newImageName);
            $data = [
                'name' => $newImageName,
                'part_original' => $originalPart."/". $newImageName,
                'part_thumbnail' => $thumbnailsPart."/". $newImageName
            ];
            return $this->sendResponse($data);
        }catch (Exception $exception){
            dd($exception->getMessage());
        }
    }

    public function removeImage(Request $request)
    {
        $fileName = $request['image'];
        $path_original = "images/contents/Original/".$fileName;
        $path_thumbnail = "images/thumbnails/".$fileName;

        if(file_exists(public_path($path_original))){
            unlink($path_original);
            unlink($path_thumbnail);
            $data = ['name' => $fileName];
            return $this->sendResponse($data);
        }
    }

    public function deleteImage(Request $request)
    {
        $fileName = $request['image'];
        $path_original = "images/contents/Original/".$fileName;

        if(file_exists(public_path($path_original))){
            unlink($path_original);
            $data = ['name' => $fileName];
            return $this->sendResponse($data);
        }
    }

}
