<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index()
    {
        $data = City::orderBy('title_en', 'asc')->get();
        return $this->sendResponse($data);
    }
}
