<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\BackendMenuCollection;
use App\Http\Resources\mainMenuCollection;
use App\Http\Resources\MenuNoParentCollection;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\UserPermission;
use App\Models\Backend\AdminMenu;
use App\Models\Backend\BackendMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MainMenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AdminMenu::where('parent_id',0)->orderBy('ordering', 'desc')->paginate(CoreFunction::config('Pagination'));
        return new mainMenuCollection($data);
    }

    public function getAllBackendMenu()
    {
        $data = AdminMenu::where('state',1)->orderBy('ordering', 'desc')->get();
        return $this->sendListResponse($data);
    }

    public function getAllParentBackendMenu()
    {
        $data = AdminMenu::where('parent_id',0)->where('state',1)->orderBy('ordering', 'desc')->get();
        return new BackendMenuCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255']
        ]);

        $request['state'] = (!empty($request['state'])) ? 1 : 0;

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = AdminMenu::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = AdminMenu::find($id);
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $request['state'] = (!empty($request['state'])) ? 1 : 0;

        $data = AdminMenu::find($id);
        $data->update($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AdminMenu::find($id);
        $data->delete();
        return $this->sendResponse($data);
    }

    public function updateOrdering(Request $request){
        $total = $request['total'];
        $menus = $request['menus'];
        foreach ($menus as $menu){
            $id = $menu['id'];
            $backendMenu = AdminMenu::find($id);
            $backendMenu->update([
                'ordering' => $total
            ]);
            $total = $total-1 ;
        }
        return $this->sendResponse($menus);
    }

    public function getMenuNoParent(){
        $menu = AdminMenu::groupBy('parent_id')->where('parent_id','>', 0)->get('parent_id');
        $data = AdminMenu::whereNotIn('id', $menu)->get();
        return new MenuNoParentCollection($data);
    }
}
