<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TourAgencyCollection;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\UserProfile;
use App\Models\Frontend\TourAgency;
use Illuminate\Http\Request;

class TourAgencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $data = TourAgency::orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        return new TourAgencyCollection($data);
    }

    public function getAllTourAgency(){
        $data = UserProfile::join('tbl_tour_agency','core_user_details.agency_id','tbl_tour_agency.id')
                             ->join('core_users_account', 'core_user_details.sec_user_id', 'core_users_account.id')
                             ->where('core_user_details.is_agency',1)
                             ->select('core_user_details.sec_user_id', 'tbl_tour_agency.*', 'core_users_account.state','core_users_account.is_mot_approved')
                             ->orderBy('core_users_account.id','desc')
                             ->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);                     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = TourAgency::join('tbl_country','tbl_tour_agency.country_id','tbl_country.id')
                ->join('core_user_details','core_user_details.agency_id','tbl_tour_agency.id')
                ->select('tbl_tour_agency.*','tbl_country.name as country_name','core_user_details.sec_user_id')
                ->where('tbl_tour_agency.id', $id)
                ->first();
        return $this->sendResponse($data);
    }

    public function getPackageByCountry($country_id)
    {
        if ($country_id > 0){
            $data = TourAgency::where('country_id',$country_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }else{
            $data = TourAgency::orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }
        return new TourAgencyCollection($data);
    }

    public function findAgency(){
        if ($search = \Request::get('q')) {
            $users = TourAgency::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%");
            })->paginate(20);
        }else{
            $users = TourAgency::orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }

        return new TourAgencyCollection($users);
    }
}
