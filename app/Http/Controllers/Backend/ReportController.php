<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TourPackageByCountryCollection;
use App\Http\Resources\tourPackageReportCollection;
use App\Libraries\Backend\CoreFunction;
use App\Models\Frontend\Tourists;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function summaryTourPackage(){
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');

        if ($fromDate != ''){
            $tourAgency = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->groupBy('agency_id')->pluck('agency_id');
            $tourPackage = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->pluck('id');

            $NumTA = TourAgency::whereIn('id', $tourAgency)->selectRaw('count(*) as NumTA')->first();
            $NumTP = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->selectRaw('count(*) as TotalNumTP')->first();
            $Tourists = Tourists::whereIn('package_id', $tourPackage)->selectRaw('count(*) as TotalTourists')->first();
        }else{
            $tourAgency = TourPackage::groupBy('agency_id')->pluck('agency_id');
            $tourPackage = TourPackage::pluck('id');

            $NumTA = TourAgency::whereIn('id', $tourAgency)->selectRaw('count(*) as NumTA')->first();
            $NumTP = TourPackage::selectRaw('count(*) as TotalNumTP')->first();
            $Tourists = Tourists::whereIn('package_id', $tourPackage)->selectRaw('count(*) as TotalTourists')->first();
        }

        $data['NumTA'] = $NumTA->NumTA;
        $data['NumTP'] = $NumTP->TotalNumTP;
        $data['Tourists'] = $Tourists->TotalTourists;

        return $this->sendResponse($data);
    }


    public function tourPackageReport(){
        $data = TourAgency::all();
        return new tourPackageReportCollection($data);
    }

    public function summaryTourPackageByCountry(){
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');

        if ($fromDate != ''){
            $tourAgencyId = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->groupBy('agency_id')->pluck('agency_id');
        }else{
            $tourAgencyId = TourPackage::groupBy('agency_id')->pluck('agency_id');
        }

        $TourAgency = TourAgency::whereIn('id', $tourAgencyId)->selectRaw('country_id')->groupBy('country_id')->paginate(CoreFunction::config('Pagination'));

        return new TourPackageByCountryCollection($TourAgency);
    }
}
