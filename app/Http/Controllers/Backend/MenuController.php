<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\menuCollection;
use App\Http\Resources\menuResource;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Frontend\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUserRole = CoreBackend::getCurrentUserRole();
        dd($currentUserRole);
        $data = Menu::where('trashed', false)->where('parent_id',0)->orderBy('id', 'desc')->paginate(CoreFunction::config('Pagination'));
        return new menuCollection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string'
        ]);

        $request['link_href'] = (!empty($request['link_href'])) ? $request['link_href'] : '#';
        $request['link_cat_id'] = (!empty($request['link_cat_id'])) ? $request['link_cat_id'] : 0;
        $request['link_catsub_id'] = (!empty($request['link_catsub_id'])) ? $request['link_catsub_id'] : 0;
        $request['link_cont_id'] = (!empty($request['link_cont_id'])) ? $request['link_cont_id'] : 0;
        $request['link_mod_id'] = (!empty($request['link_mod_id'])) ? $request['link_mod_id'] : 0;
        $request['state'] = (!empty($request['state'])) ? 1 : 0;
        $request['target'] = (!empty($request['target'])) ? 1 : 0;
        $request['ordering'] = 0;
        $request['created_by'] = auth()->user()->id;
        $request['trashed']= false;

        $data = Menu::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Menu::find($id);
        return $this->sendResponse($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string'
        ]);

        $request['link_href'] = (!empty($request['link_href'])) ? $request['link_href'] : '#';
        $request['link_cat_id'] = (!empty($request['link_cat_id'])) ? $request['link_cat_id'] : 0;
        $request['link_catsub_id'] = (!empty($request['link_catsub_id'])) ? $request['link_catsub_id'] : 0;
        $request['link_cont_id'] = (!empty($request['link_cont_id'])) ? $request['link_cont_id'] : 0;
        $request['link_mod_id'] = (!empty($request['link_mod_id'])) ? $request['link_mod_id'] : 0;
        $request['state'] = (!empty($request['state'])) ? 1 : 0;
        $request['target'] = (!empty($request['target'])) ? 1 : 0;
        $request['updated_by'] = auth()->user()->id;

        $menu = Menu::findOrFail($id);
        $data = $menu->update($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function trashed($id){
        $data = Menu::where('id', $id)->update([
            'trashed' => '1'
        ]);
        return $this->sendResponse($data);
    }

    public function getParentMenu(){
        $data = Menu::where('parent_id', 0)->where('trashed',0)->get();
        return $this->sendResponse($data);
    }

    public function getSubParentMenu($id){
        $data = Menu::where('parent_id', $id)->where('trashed',0)->get();
        return $this->sendResponse($data);
    }
}
