<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TouristsCollection;
use App\Http\Resources\TourAgencyCollection;
use App\Libraries\Backend\CoreFunction;
use App\Models\Frontend\Tourists;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Request;

class TouristsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');
        if (($fromDate == '')||($toDate == '')){
            $data = Tourists::orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }else{
            $package = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->pluck('id');
            $data = Tourists::whereIn('package_id', $package)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }
        return new TouristsCollection($data);
    }

    public function searchTouristsBackend($agency_id){
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');
        if (($fromDate == '')||($toDate == '')){
            $data = Tourists::where('agency_id', $agency_id)->where('type','TOURISTS')->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }else{
            $package = TourPackage::whereBetween('arrival_date',[$fromDate, $toDate])->pluck('id');
            $data = Tourists::whereIn('package_id', $package)->where('agency_id', $agency_id)->where('type','TOURISTS')->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }
        return new TouristsCollection($data);
    }

    public function getTouristsBackend($id)
    {
        $data = Tourists::where('package_id', $id)->where('type','TOURISTS')->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        return new TouristsCollection($data);
    }

    public function get_tourists_by_package_id($id)
    {
        $data = Tourists::where('package_id', $id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        return new TouristsCollection($data);
    }

    public function findTourists(){
        if ($search = \Request::get('q')) {
            $users = Tourists::where(function($query) use ($search){
                $query->where('type','TOURISTS')
                    ->where('passport','LIKE',"%$search%")
                    ->orWhere('last_name','LIKE',"%$search%")
                    ->orWhere('first_name','LIKE',"%$search%");
            })->orderBy('id','desc')->paginate(20);
        }else{
            $users = Tourists::where('type','TOURISTS')->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        }

        return new TouristsCollection($users);
    }

}
