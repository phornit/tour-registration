<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamLeaderCollection;
use App\Http\Resources\TeamLeaderResource;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\UserAccount;
use App\Models\Auth\UserProfile;
use App\Models\Frontend\TeamLeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TeamleaderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agency_id = CoreBackend::currentAgencyId();

        $data = UserProfile::where('agency_id', $agency_id)->where('is_team_leader', true)->paginate(CoreFunction::config('Pagination'));
        return new TeamLeaderCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'passport' => 'required|string|max:191',
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'gender' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $agency_id = CoreBackend::currentAgencyId();
        
        $data = UserProfile::create([
            'passport' => $request['passport'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'gender' => $request['gender'],
            'date_of_birth' => $request['dob'],
            'phone' => $request['phone'],
            'nationality' => $request['nationality'],
            'address' => null,
            'company' => $request['company'],
            'position' => null,
            'skill' => $request['skill'],
            'photo' => $request['photo'],
            'agency_id' => $agency_id,
            'sec_user_id' => 0,
            'is_team_leader' => true,
            'is_agency' => false
        ]);

        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = UserProfile::find($id);
        return new TeamLeaderResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'passport' => 'required|string|max:191',
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
        ]);

        $data = UserProfile::find($id);
        $data->update($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = UserProfile::find($id)->delete();
        return $this->sendResponse($data);
    }
}
