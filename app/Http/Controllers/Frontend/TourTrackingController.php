<?php

namespace App\Http\Controllers\Frontend;

use App\Events\MyEvent;
use App\Events\TourTrackingEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\TourPackageCollection;
use App\Http\Resources\TourPackageTrackingCollection;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Frontend\TourPackage;
use App\Models\Frontend\TourPackageTracking;
use App\Models\Frontend\TourPackageTrackingPhoto;
use App\Models\Frontend\TourPackageTrackingTourist;
use Illuminate\Http\Request;

class TourTrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agency_id = CoreBackend::currentAgencyId();
        $data = TourPackageTracking::where('agency_id', $agency_id)->orderBy('id', 'desc')->paginate(CoreFunction::config('Pagination'));
        return new TourPackageTrackingCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'message' => 'required'
        ]);
        $request['agency_id'] = CoreBackend::currentAgencyId();

        $data = TourPackageTracking::create($request->all());

        if($data){
            if(count($request['tourists_id']) > 0){
                foreach($request['tourists_id'] as $value){
                    TourPackageTrackingTourist::create([
                        'tracking_id' => $data->id,
                        'tourist_id' => $value['id'],
                        'tourist_name' => $value['full_name'],
                    ]);
                }
            }

            if(count($request['photo']) > 0){
                foreach($request['photo'] as $value){
                    TourPackageTrackingPhoto::create([
                        'tracking_id' => $data->id,
                        'photo' => $value['name'],
                    ]);
                }
            }
        }

        // event(new MyEvent($data));
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TourPackageTracking::find($id);
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'message' => 'required'
        ]);
        $data = TourPackageTracking::find($id);
        $data->update($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TourPackageTracking::find($id)->delete();
        return $this->sendResponse($data);
    }

    public function getTourPackageRegister(){
        $agency_id = CoreBackend::currentAgencyId();
        $data = TourPackage::where('agency_id', $agency_id)->where('is_register',1)->get();
        return new TourPackageCollection($data);
    }
}
