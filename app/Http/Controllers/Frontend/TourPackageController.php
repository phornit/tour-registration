<?php

namespace App\Http\Controllers\Frontend;

use App\Enums\TourPackageStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\TourPackageCollection;
use App\Http\Resources\TourPackageGroupByDaysResource;
use App\Http\Resources\TourPackageNotRegisterCollection;
use App\Http\Resources\TourPackageRegisterCollection;
use App\Http\Resources\TourPackageResource;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Frontend\Itinerary;
use App\Models\Frontend\ItineraryActivity;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TourPackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agency_id = CoreBackend::currentAgencyId();
        $data = TourPackage::where('agency_id',$agency_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        return new TourPackageCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:191',
            'arrival_date' => 'required',
            'departure_date' => 'required',
            'market' => 'required',
            'leader_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }
        
        $request['agency_id'] = CoreBackend::currentAgencyId();
        $request['is_approved'] = false;
        $data = TourPackage::create($request->all());

        if ($data){
            if(count($request['Itinerary']) > 0){
                $i = 1;
                foreach($request['Itinerary'] as $value){

                    $Itinerary = Itinerary::create([
                        'package_id' => $data->id,
                        'days' => $i,
                        'date_start' => $value['date_start'],
                        'time_start' => $value['time_start'],
                        'title' => $value['title'],
                        'description' => $value['description']
                    ]);

                    if(count($value['activities']) > 0){
                        foreach($value['activities'] as $row){
                            $safe_cate = explode(',', $row['safe_cate']);
                            $safe_loc = explode(',', $row['safe_loc']);
                            ItineraryActivity::create([
                                'package_id' => $data->id,
                                'itinerary_id' => $Itinerary->id,
                                'safe_cate_id' => $safe_cate[0],
                                'safe_cate_name' => $safe_cate[1],
                                'safe_loc_id' => $safe_loc[0],
                                'safe_loc_name' => $safe_loc[1],
                                'location_id' => $row['location_id'],
                                'time' => $row['time'],
                                'activity' => $row['activity'],
                                'detail' => $row['detail']
                            ]);
                        }
                    }
                    $i++;
                }
            }
        }
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TourPackage::find($id);
        return new TourPackageResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'arrival_date' => 'required'
        ]);
        $data = TourPackage::find($id);
        $data->update($request->all());

        if ($data){
            if(count($request['Itinerary']) > 0){

                $i = 1;
                Itinerary::where('package_id', $id)->delete();
                ItineraryActivity::where('package_id', $id)->delete();

                foreach($request['Itinerary'] as $value){

                    $Itinerary = Itinerary::create([
                        'package_id' => $data->id,
                        'days' => $i,
                        'date_start' => $value['date_start'],
                        'time_start' => $value['time_start'],
                        'title' => $value['title'],
                        'description' => $value['description']
                    ]);

                    if(count($value['activities']) > 0){
                        foreach($value['activities'] as $row){
                            $safe_cate = explode(',', $row['safe_cate']);
                            $safe_loc = explode(',', $row['safe_loc']);
                            ItineraryActivity::create([
                                'package_id' => $data->id,
                                'itinerary_id' => $Itinerary->id,
                                'safe_cate_id' => $safe_cate[0],
                                'safe_cate_name' => $safe_cate[1],
                                'safe_loc_id' => $safe_loc[0],
                                'safe_loc_name' => $safe_loc[1],
                                'location_id' => $row['location_id'],
                                'time' => $row['time'],
                                'activity' => $row['activity'],
                                'detail' => $row['detail']
                            ]);
                        }
                    }
                    $i++;
                }
            }
        }

        return $this->sendResponse($data);
    }

    public function update_register_data(Request $request, $id){
        $data = TourPackage::find($id);
        $data->update([
            'is_register' => 1
        ]);
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Itinerary::where('package_id', $id)->delete();
        $data = TourPackage::findOrFail($id)->delete();
        return $this->sendResponse($data);
    }

    public function updateTourPackageStatus($id){
        $state = \Request::get('state');
        $data = TourPackage::find($id);
        $data->update([
            'state' =>  $state
        ]);
        return $this->sendResponse($data);
    }

    public function getTourPackageRegister(){
        $agency_id = CoreBackend::currentAgencyId();
        $data = TourPackage::where('agency_id', $agency_id)->where('is_register', 1)->where('is_approved',1)->get();
        return new TourPackageRegisterCollection($data);
    }

    public function getTourPackageNotRegister(){
        $agency_id = CoreBackend::currentAgencyId();
        $data = TourPackage::where('agency_id', $agency_id)->where('is_approved',0)->get();
        return new TourPackageNotRegisterCollection($data);
    }

    public function getTourPackageGroupByDays($id)
    {
        $data = TourPackage::find($id);
        return new TourPackageGroupByDaysResource($data);
    }
}
