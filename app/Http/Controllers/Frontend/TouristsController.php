<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TouristsCollection;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\UserProfile;
use App\Models\Frontend\Tourists;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class TouristsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agency_id = CoreBackend::currentAgencyId();
        $data = Tourists::where('agency_id', $agency_id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        return new TouristsCollection($data);
    }
    public function get_tourists_by_package_id($id)
    {
        $agency_id = CoreBackend::currentAgencyId();
        $data = Tourists::where('agency_id', $agency_id)->where('package_id', $id)->orderBy('id','desc')->paginate(CoreFunction::config('Pagination'));
        return new TouristsCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'passport' => 'required|string|max:191',
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
        ]);
        $request['agency_id'] = CoreBackend::currentAgencyId();
        $data = Tourists::create($request->all());

        return $this->sendResponse($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Tourists::find($id);
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'passport' => 'required|string|max:191',
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
        ]);

        $data = Tourists::find($id);
        $data->update($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Tourists::find($id)->delete();
        return $this->sendResponse($data);
    }
}
