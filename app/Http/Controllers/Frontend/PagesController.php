<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return view('pages');
    }

    public function register()
    {
        return view('register');
    }

    public function blank()
    {
        return view('blank');
    }
}
