<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Resources\TourAgencyResource;
use App\Http\Resources\TourPackageResource;
use App\Libraries\Backend\CoreBackend;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\UserProfile;
use App\Models\Frontend\TourAgency;
use Illuminate\Http\Request;

class TourAgencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = TourAgency::orderBy('agency_id','desc')->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191'
        ]);
        $agency_id = CoreBackend::currentAgencyId();
        $request['agency_id'] = $agency_id;
        $data = TourAgency::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAgency()
    {
        $agency_id = CoreBackend::currentAgencyId();
        $data = TourAgency::where('id', $agency_id)->first();
        return new TourAgencyResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191'
        ]);
        $data = TourAgency::where('id', $id);
        $data->update($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
