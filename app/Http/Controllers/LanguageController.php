<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class LanguageController extends Controller
{
    public function language_backend($locale)
    {
        \Session::put('locale', $locale);

        return Redirect::back();
    }

    public function language_frontend()
    {
        \Session::put('locale', \Request::get('locale'));
        $next_url = \Request::get('url');
        return redirect($next_url);
    }

    public function language_trans($langid)
    {
        \Session::put('clangid', $langid);
        \Session::put('dlangid', self::getLanguageID(\Config::get('app.fallback_locale')));

        return Redirect::back();
    }

    static function getLanguageID ($locale)
    {
        $query = DB::table('core_languages');
        $query->where('localization', $locale);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->id;
        }
    }
}
