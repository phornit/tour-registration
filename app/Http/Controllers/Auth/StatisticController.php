<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\Statistic;
use App\Models\Frontend\Learning;

class StatisticController extends Controller
{
    public function index()
    {
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');
        if ($fromDate !='--- All ---') {
            $data = Statistic::whereBetween('created_at',[$fromDate, $toDate])->orderBy('id', 'desc')->paginate(CoreFunction::config('Pagination'));
        }else{
            $data = Statistic::latest()->paginate(CoreFunction::config('Pagination'));
        }
        return $this->sendListResponse($data);
    }

    public function uniqueVisitor(){
        $unique['unique_visitor'] = Statistic::distinct('ip_address')->count('ip_address');
        $unique['visitor'] = Statistic::count('ip_address');

        $unique['unique_elearning'] = Learning::distinct('user_id')->count('user_id');
        $unique['elearning'] = Learning::count('user_id');

        return $this->sendResponse($unique);
    }
}
