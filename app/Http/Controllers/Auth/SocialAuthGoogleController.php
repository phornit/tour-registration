<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Frontend\TourAgency;
use App\User;
use Socialite;
use Exception;
use Auth;

class SocialAuthGoogleController extends Controller
{
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $getUser = Socialite::driver('google')->user();
            $user = $this->createUser($getUser);
            $role = $user->user_role;
            Auth::loginUsingId($user->id);
            if ($role == 3){
                return redirect()->intended('/tour-agency-profile');
            }else{
                return redirect()->intended('/admin/dashboard');
            }
        } catch (Exception $e) {
            return redirect('/auth/google');
        }
    }

    function createUser($user){
        $data = User::where('google_id', $user->id)->first();
        if (!$data){
            $data = User::create([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'user_role' => 3,
                'state' => true,
                'trashed' => false,
                'google_id' => $user->id,
            ]);

            TourAgency::create([
                'agency_id' => $data->id,
                'name' => $user->getName(),
                'email' => $user->getEmail(),
            ]);
        }
        return $data;
    }
}
