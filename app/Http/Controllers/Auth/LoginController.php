<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Auth\Role;
use App\Models\Auth\UserAccount;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    public function authenticate(Request $request)
    {
        $active = UserAccount::where('email', $request->username) -> where('state', 1)-> where('is_mot_approved', 1) -> exists();
        if($active == false)
            return $this->sendInvalidResponse('This user is not active or approved');

        $request->request->add([
            'username' => $request->username,
            'password' => $request->password,
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => '*',
            'state' => true,
            'is_mot_approved' => true,
        ]);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        $data = \Route::dispatch($proxy);

        return $data;
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $cookie = Cookie::forget('_token');
        return response()->json([
            'access_token' => null,
            'statusCode' => 200,
            'message' => 'Success'
        ])->withCookie($cookie);
    }

    // public function authenticate(Request $request)
    // {
    //     $credentials = array(
    //         'email' => strtolower($request['email']),
    //         'password' => $request['password'],
    //         'state' => 1
    //     );
    //     if (Auth::attempt($credentials)) {
    //         $remember = $request['remember'];
    //         $role = null;
    //         if (!empty($remember)){
    //             $user = User::find(Auth::user()->id);
    //             $role = $user->user_role;
    //             Auth::login($user, true);
    //             Cookie::queue('remember', true, 50);
    //             Cookie::queue('email', $request['email'], 50);
    //             Cookie::queue('password', $request['password'], 50);
    //         }

    //         Cookie::queue('remember', false, 50);

    //         if ($role == 1){
    //             return redirect()->intended('/admin/dashboard');
    //         }else{
    //             return redirect()->intended('/tour-agency-profile');
    //         }

    //     }else{
    //         return Redirect::to('/login')
    //             ->with([
    //                 'message' => '<div class="col-md-12">
    //                     <div class="alert alert-danger alert-dismissable">
    //                         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    //                         Email or password incorrect! Please try again.</div>
    //                 </div>',
    //             ]);
    //     }
    // }
}
