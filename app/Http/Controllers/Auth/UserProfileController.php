<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\getCurrentUserResource;
use App\Models\Auth\UserAccount;
use App\Models\Auth\UserProfile;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserProfileController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('core_users_account')
            ->leftJoin('core_user_details', 'core_users_account.id', '=', 'core_user_details.user_account_id')
            ->select('core_users_account.*', 'core_user_details.*')
            ->where('core_users_account.id', '=', auth()->user()->id)
            ->first();

        return $this->sendResponse($data);
    }

    public function getCurrentUser(){
        $data = UserAccount::find(auth()->user()->id);
        return new getCurrentUserResource($data);
    }

    public function createUserProfile(Request $request, $sec_user_id)
    {
        $request['sec_user_id'] = $sec_user_id;
        $data = UserProfile::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUserProfile(Request $request, $id)
    {
        $userProfile = UserProfile::where('sec_user_id',$id);
        $data = $userProfile->update($request->all());
        return $this->sendResponse($data);
    }

    public function update(Request $request, $id)
    {
        $userProfile = UserProfile::where('id',$id);
        $data = $userProfile->update($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = UserProfile::where('sec_user_id',$id)->first();
        return $this->sendResponse($data);
    }

    
    public function changePassword(Request $request){
        $credentials = array(
            'email' => strtolower($request['email']),
            'password' => $request['password'],
        );
        if (Auth::guard('web')->attempt($credentials)) {
            $user = User::find(Auth::user()->id);
            $user->update([
                'password' => Hash::make($request['new_password'])
            ]);
            return $this->sendResponse($user);
        }else{
            return $this->sendInvalidResponse("Invalid Password");
        }
    }

    public function login()
    {
        $data['url'] = \Request::get('url');
        $data['remember'] = Cookie::get('remember');
        $data['email'] = Cookie::get('email');
        $data['password'] = Cookie::get('password');
        return view('auth/login', $data);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $cookie = Cookie::forget('_token');
        return response()->json([
            'access_token' => null,
            'statusCode' => 200,
            'message' => 'Success'
        ])->withCookie($cookie);
    }
}
