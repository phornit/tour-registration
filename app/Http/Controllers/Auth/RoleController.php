<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\roleResource;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\Role;
use App\Models\Auth\UserPermission;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Role::orderBy('id', 'desc')->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191|unique:core_user_roles'
        ]);

        $request['created_by'] = auth()->user()->id;
        $request['state'] = (!empty($request['state'])) ? 1 : 0;
        $request['trashed']= false;
        $data = Role::create($request->all());

        if($data){
            foreach($request['menus'] as $value){
                $userPermission = new UserPermission;
                $userPermission->user_roles_id = $data->id;
                $userPermission->menu_id = $value;
                $userPermission->save();
            }
        }

        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Role::find($id);
        return new roleResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roles = Role::findOrFail($id);

        $this->validate($request,[
            'name' => 'required|string|max:191|unique:core_user_roles,name,'.$roles->id,
        ]);

        $request['updated_by'] = auth()->user()->id;
        $data = $roles ->update($request->all());

        if($data){
            $permission = UserPermission::where('user_roles_id',$id);
            $permission->delete();

            foreach($request['menus'] as $value){
                $userPermission = new UserPermission;
                $userPermission->user_roles_id = $id;
                $userPermission->menu_id = $value;
                $userPermission->save();
            }
        }


        return $this->sendResponse($roles);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $data = $role->delete();

        if ($data){
            $permission = UserPermission::where('user_roles_id',$id);
            $permission->delete();
        }

        return $this->sendResponse($data);
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $users = Role::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%");
            })->paginate(20);
        }else{
            $users = Role::latest()->paginate(CoreFunction::config('Pagination'));
        }

        return $users;

    }
}
