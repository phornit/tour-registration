<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendMailVerifyEmail;
use App\Models\Auth\UserAccount;
use App\Models\Auth\UserProfile;
use App\Models\Frontend\TourAgency;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::REGISTER;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:core_users_account'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function UserRegister(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:core_users_account'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $userAccount = UserAccount::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'user_role' => 3, // 3 = Role Agency
            'is_admin' => false,
            'is_mot_approved' => false,
            'state' => false,
            'trashed' => false
        ]);

        if($userAccount){

            $agency = TourAgency::create([
                'name' => $request['agency_name'],
                'email' => $request['email'],
                'description' => $request['description'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'address' => $request['address'],
                'country_id' => $request['country_id'],
            ]);

            if($agency){
                $userProfile = UserProfile::create([
                    'passort' => null,
                    'first_name' => $request['name'],
                    'last_name' => null,
                    'gender' => null,
                    'date_of_birth' => null,
                    'phone' => null,
                    'nationality' => null,
                    'address' => null,
                    'company' => null,
                    'position' => null,
                    'skill' => null,
                    'photo' => null,
                    'agency_id' => $agency->id,
                    'sec_user_id' => $userAccount->id,
                    'is_agency' => true,
                    'is_team_leader' => false,
                ]);

                if($userProfile){
                    Mail::to($request['email'])->queue(new SendMailVerifyEmail($userAccount));
                    return $this->sendResponse($userAccount);
                }
            }
        }
        return $this->sendInvalidResponse('Register Fail!');
    }

    public function verifyAccount($id){
        $user_id = Crypt::decrypt($id);
        $user = UserAccount::where('id', $user_id)->update([
           'state' => 1,
        ]);

        if ($user){
            return redirect('/?verify=true');
        }else{
            return redirect('?verify=false');
        }
    }
}
