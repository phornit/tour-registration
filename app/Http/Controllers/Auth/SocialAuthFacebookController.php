<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Frontend\TourAgency;
use App\User;
use Socialite;
use Exception;
use Auth;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
            $getUser = Socialite::driver('facebook')->user();
            $user = $this->createUser($getUser);
            $role = $user->user_role;
            Auth::loginUsingId($user->id);
            if ($role == 3){
                return redirect()->intended('/tour-agency-profile');
            }else{
                return redirect()->intended('/admin/dashboard');
            }
        } catch (Exception $e) {

            return redirect('/auth/facebook');
        }
    }

    function createUser($user){
        $data = User::where('facebook_id', $user->id)->first();
        if (!$data){
            $data = User::create([
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'user_role' => 3,
                'state' => true,
                'trashed' => false,
                'facebook_id' => $user->id,
            ]);

            TourAgency::create([
                'agency_id' => $data->id,
                'name' => $user->getName(),
                'email' => $user->getEmail(),
            ]);
        }
        return $data;
    }
}
