<?php

namespace App\Http\Resources;

use App\Models\Backend\Country;
use Illuminate\Http\Resources\Json\JsonResource;

class TourAgencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'logo' => $this->logo,
            'country_id' => $this->country_id,
            'country' => Country::where('id',$this->country_id)->pluck('name')->first()
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
