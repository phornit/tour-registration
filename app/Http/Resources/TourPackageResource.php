<?php

namespace App\Http\Resources;

use App\Models\Backend\Country;
use App\Models\Frontend\Itinerary;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Resources\Json\JsonResource;

class TourPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'agency_id' => $this->agency_id,
            'agency' => TourAgency::where('id',$this->agency_id)->pluck('name')->first(),
            'leader_id' => $this->leader_id,
            'title' => $this->title,
            'arrival_date' => $this->arrival_date,
            'departure_date' => $this->departure_date,
            'market' => $this->market,
            'market_name' => Country::where('id', $this->market)->pluck('name')->first(),
            'num_day_tour' => $this->num_day_tour,
            'meal' => $this->meal,
            'transport' => $this->transport,
            'activity' => $this->activity,
            'description' => $this->description,
            'state' => TourPackage::TourPackageAllState($this->id),
            'reason' => $this->reason,
            'price' => $this->price,
            'hotel_qarantine' => $this->hotel_qarantine,
            'insurance_company' => $this->insurance_company,
            'Itinerary' => Itinerary::getItenerary($this->id),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
