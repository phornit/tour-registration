<?php

namespace App\Http\Resources;

use App\Models\Backend\Country;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TourAgencyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'agency_id' => $page->agency_id,
                    'name' => $page->name,
                    'description' => $page->description,
                    'email' => $page->email,
                    'phone' => $page->phone,
                    'address' => $page->address,
                    'logo' => $page->logo,
                    'country_id' => $page->country_id,
                    'country' => Country::where('id',$page->country_id)->pluck('name')->first()
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
