<?php

namespace App\Http\Resources;

use App\Models\Backend\Country;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TourPackageRegisterCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'agency_id' => $page->agency_id,
                    'market' => Country::where('id',$page->market)->pluck('name')->first(),
                    'agency' => TourAgency::where('id', $page->agency_id)->pluck('name')->first(),
                    'leader_id' => $page->leader_id,
                    'title' => $page->title,
                    'arrival_date' => $page->arrival_date,
                    'departure_date' => $page->departure_date,
                    'num_day_tour' => $page->num_day_tour,
                    'meal' => $page->meal,
                    'transport' => $page->transport,
                    'activity' => $page->activity,
                    'description' => $page->description,
                    'is_register' => $page->is_register,
                    'is_approved' => $page->is_approved,
                    'state' => TourPackage::TourPackageRegisterState($page->id)
                ];
            }),
        ];
    }


    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
