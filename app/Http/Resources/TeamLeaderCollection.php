<?php

namespace App\Http\Resources;

use App\Models\Auth\UserAccount;
use App\Models\Backend\Country;
use App\Models\Frontend\TourAgency;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TeamLeaderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'agency_id' => $page->agency_id,
                    'agency' => isset($page->agency_id) ? TourAgency::where('id',$page->agency_id)->pluck('name')->first() : '',
                    'passport' => $page->passport,
                    'first_name' => $page->first_name,
                    'last_name' => $page->last_name,
                    'gender' => $page->gender,
                    'date_of_birth' => $page->date_of_birth,
                    'phone' => $page->phone,
                    'email' => UserAccount::where('id',$page->sec_user_id)->pluck('email')->first(),
                    'nationality' => isset($page->nationality) ? Country::where('id', $page->nationality)->pluck('nationality')->first() : '',
                    'photo' => $page->photo,
                    'tour_guide' => $page->position,
                    'position' => $page->position,
                    'country' => isset($page->country_id) ? Country::where('id', $page->country_id)->pluck('name')->first() : '',
                    'sec_user_id' => $page->sec_user_id
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
