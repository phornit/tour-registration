<?php

namespace App\Http\Resources;

use App\Models\Auth\UserAccount;
use App\Models\Backend\Country;
use App\Models\Frontend\TourAgency;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamLeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'agency_id' => $this->agency_id,
            'agency' => isset($this->agency_id) ? TourAgency::where('id',$this->agency_id)->pluck('name')->first() : '',
            'passport' => $this->passport,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'gender' => $this->gender,
            'date_of_birth' => $this->date_of_birth,
            'phone' => $this->phone,
            'email' => UserAccount::where('id',$this->sec_user_id)->pluck('email')->first(),
            'nationality' => $this->nationality,
            'nationality_name' => isset($this->nationality) ? Country::where('id', $this->nationality)->pluck('nationality')->first() : '',
            'photo' => $this->photo,
            'skill' => $this->skill,
            'position' => $this->position,
            'company' => $this->company,
            'country' => isset($this->country_id) ? Country::where('id', $this->country_id)->pluck('name')->first() : ''
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
