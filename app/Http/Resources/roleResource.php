<?php

namespace App\Http\Resources;

use App\Models\Auth\UserPermission;
use App\Models\Backend\AdminMenu;
use Illuminate\Http\Resources\Json\JsonResource;

class roleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'state' => $this->state,
            'menus' => $this->rolePermission($this->id)
        ];
    }

    public function rolePermission($role_id){
        $adminMenus = UserPermission::join('core_backend_menus', 'core_user_permission.menu_id', '=', 'core_backend_menus.id')
            ->select('core_backend_menus.*')
            ->where('user_roles_id', $role_id)->get();
        return $adminMenus;
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
