<?php

namespace App\Http\Resources;

use App\Models\Backend\Country;
use App\Models\Frontend\Tourists;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TourPackageByCountryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'country_id' => $page->country_id,
                    'country' => Country::where('id', $page->country_id)->pluck('name')->first(),
                    'NumTA' => self::TourAgencyByCountry($page->country_id),
                    'NumTP' => self::TourPackageByCountry($page->country_id),
                    'NumTourists' => $this->TouristsByCountry($page->country_id),
                ];
            }),
        ];
    }

    static function TourAgencyByCountry($country_id){
        $Agency = TourAgency::where('country_id', $country_id)->selectRaw('count(*) as TotalNumTA')->first();
        return $Agency->TotalNumTA;
    }

    static function TourPackageByCountry($country_id){
        $Agency = TourAgency::where('country_id', $country_id)->groupBy('id')->pluck('id');
        $Package = TourPackage::whereIn('agency_id',$Agency)->selectRaw('count(*) as TotalNumTP')->first();
        return $Package->TotalNumTP;
    }

    public function TouristsByCountry($country_id){
        $Agency = TourAgency::where('country_id', $country_id)->groupBy('id')->pluck('id');
        $Package = TourPackage::whereIn('agency_id',$Agency)->pluck('id');
        $Tourists = Tourists::whereIn('package_id',$Package)->selectRaw('count(*) as TotalNumTourists')->first();
        return $Tourists->TotalNumTourists;
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
