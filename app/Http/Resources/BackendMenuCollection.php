<?php

namespace App\Http\Resources;

use App\Models\Backend\AdminMenu;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BackendMenuCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'parent_id' => $page->parent_id,
                    'name' => $page->name,
                    'link' => $page->link,
                    'class' => $page->class_name,
                    'state' => $page->state,
                    'color' => $page->color,
                    'ordering' => $page->ordering,
                    'class_name' => $page->class_name,
                    'subMenu' => AdminMenu::where('parent_id', $page->id)->get(),
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
