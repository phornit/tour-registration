<?php

namespace App\Http\Resources;

use App\Models\Frontend\Itinerary;
use App\Models\Frontend\TourAgency;
use Illuminate\Http\Resources\Json\JsonResource;

class TourPackageGroupByDaysResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'agency_id' => $this->agency_id,
            'agency' => TourAgency::where('id',$this->agency_id)->pluck('name')->first(),
            'leader_id' => $this->leader_id,
            'title' => $this->title,
            'arrival_date' => $this->arrival_date,
            'departure_date' => $this->departure_date,
            'traveller_return_date' => $this->traveller_return_date,
            'num_day_tour' => $this->num_day_tour,
            'meal' => $this->meal,
            'transport' => $this->transport,
            'activity' => $this->activity,
            'description' => $this->description,
            'state' => $this->state,
            'reason' => $this->reason,
            'days' => Itinerary::getItineraryBroupByDays($this->id),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
