<?php

namespace App\Http\Resources;

use App\Models\Backend\Country;
use App\Models\Frontend\TourAgency;
use App\Models\Frontend\TourPackage;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TouristsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'agency_id' => $page->agency_id,
                    'agency' => TourAgency::where('id',$page->agency_id)->pluck('name')->first(),
                    'package_id' => $page->package_id,
                    'package' => TourPackage::where('id',$page->package_id)->pluck('title')->first(),
                    'arrival_date' => TourPackage::where('id',$page->package_id)->pluck('arrival_date')->first(),
                    'passport' => $page->passport,
                    'first_name' => $page->first_name,
                    'last_name' => $page->last_name,
                    'full_name' => $page->first_name.' '.$page->last_name,
                    'gender' => $page->gender,
                    'dob' => $page->dob,
                    'phone' => $page->phone,
                    'email' => $page->email,
                    'nationality' => Country::where('id', $page->nationality)->pluck('nationality')->first(),
                    'country_id' => $page->country_id,
                    'photo' => $page->photo,
                    'country' => Country::where('id', $page->country_id)->pluck('name')->first(),
                    'travel_from' => Country::where('id', $page->travel_from)->pluck('name')->first()
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
