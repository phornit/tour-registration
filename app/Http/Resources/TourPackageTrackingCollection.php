<?php

namespace App\Http\Resources;

use App\Models\Frontend\TourPackageTrackingPhoto;
use App\Models\Frontend\TourPackageTrackingTourist;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TourPackageTrackingCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'agency_id' => $page->agency_id,
                    'package_id' => $page->package_id,
                    'message' => $page->message,
                    'state' => $page->state,
                    'tourist' => TourPackageTrackingTourist::where('tracking_id', $page->id)->get(),
                    'photo' => TourPackageTrackingPhoto::where('tracking_id', $page->id)->get(),
                ];
            }),
        ];
    }
}
