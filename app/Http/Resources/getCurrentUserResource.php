<?php

namespace App\Http\Resources;

use App\Models\Auth\UserProfile;
use App\Models\Frontend\TourAgency;
use Illuminate\Http\Resources\Json\JsonResource;

class getCurrentUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_role' => $this->user_role,
            'name' => $this->name,
            'email' => $this->email,
            'state' => $this->state,
            'is_admin' => $this->is_admin,
            'is_agency' => $this->is_agency,
            'is_team_leader' => $this->is_team_leader,
            'profile' => $this->profile($this->id),
            'agency' => isset($this->is_agency) ? $this->agency($this->id) : null,
        ];
    }

    public function profile($sec_user_id){
        $data = UserProfile::where('sec_user_id', $sec_user_id)->first();
        return $data;
    }

    public function agency($sec_user_id){
        $usreProfile = UserProfile::where('sec_user_id', $sec_user_id)->first();
        $data = TourAgency::find($usreProfile->agency_id);
        return $data;
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
