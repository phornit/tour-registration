<?php

namespace App\Http\Middleware;

use App\Modules\Auth\Language_Switcher;
use Closure;
use Illuminate\View\View;

class LanguageSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('frontend_language', Language_Switcher::build_language_switcher());

        return $next($request);
    }
}
