<?php

namespace App\Events;

use App\Models\Frontend\TourPackageTracking;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TourTrackingEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $post;

    public function __construct(TourPackageTracking $post)
    {
        $this->post = $post;
    }

    public function broadcastOn()
    {  
        return new PrivateChannel('TourPackageTracking.created'); 
    }

}
