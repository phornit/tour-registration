<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TourPackageStatus extends Enum
{
    const ACTIVE =   'ACTIVE';
    const COMING =   'COMING';
    const SNEW = 'NEW';
    const EXPIRED = 'EXPIRED';
    const PENDING = 'PENDING';
    const ALLSTATE = ['ACTIVE', 'COMING', 'NEW', 'EXPIRED', 'PENDING'];
}
