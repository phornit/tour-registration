<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'core_user_roles';

    protected $fillable = [
        'id','name','state', 'description','created_by', 'updated_by', 'trashed', 'trashed_by', 'trashed_at'
    ];
}
