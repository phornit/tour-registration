<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $table = 'core_statistic';

    protected $fillable = [
        'id','ip_address','browser', 'page','day'
    ];

    public static function pageViews($page){
        $ip_address = request()->ip();
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $day = date('Y-m-d');
        Statistic::create([
            'ip_address' => $ip_address,
            'browser' => $browser,
            'page' => $page,
            'day' => $day
        ]);
    }
}
