<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserAccount extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'core_users_account';

    protected $fillable = [
        'facebook_id',
        'google_id',
        'name', 
        'email', 
        'password', 
        'user_role', 
        'state', 
        'is_admin', 
        'is_mot_approved',
        'trashed', 
        'trashed_by', 
        'trashed_at'
    ];
}
