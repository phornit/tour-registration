<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserProfile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'core_user_details';

    protected $fillable = [
        'passport',
        'first_name', 
        'last_name', 
        'gender', 
        'date_of_birth', 
        'phone', 
        'nationality',  
        'address', 
        'company', 
        'position', 
        'skill',
        'agency_id',
        'sec_user_id',
        'photo',
        'is_agency',
        'is_team_leader'
    ];
}
