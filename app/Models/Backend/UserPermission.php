<?php

namespace App\Models\Backend;

use App\Libraries\Backend\CoreBackend;
use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table = "core_user_permission";

    protected $fillable = [
        'user_roles_id',
        'menu_id',
    ];

    static function viewPermission($menu_id){
        $currentUserRole = CoreBackend::getCurrentUserRole();
        $data = UserPermission::where('menu_id', $menu_id)->where('user_roles_id',$currentUserRole)-> exists();
        if($data){
            return true;
        }else{
            return false;
        }
    }
}
