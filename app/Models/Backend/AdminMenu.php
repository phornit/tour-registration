<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    protected $table = "core_backend_menus";
    public $timestamps = false;
    protected $fillable = [
        'name',
        'parent_id',
        'link',
        'class_name',
        'color',
        'state',
        'ordering'
    ];

    static function subMenus($menu_id){
        $AdminMenu = AdminMenu::where('parent_id', $menu_id)->orderBy('ordering', 'desc')->get();
        $data = $AdminMenu->map(function($page){
            return [
                'id' => $page->id,
                'parent_id' => $page->parent_id,
                'name' => $page->name,
                'link' => $page->link,
                'class_name' => $page->class_name,
                'state' => $page->state,
                'color' => $page->color,
                'ordering' => $page->ordering,
                'views' => UserPermission::viewPermission($page->id)
            ];
        });

        return $data;
    }
}
