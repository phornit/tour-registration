<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'tbl_country';
    protected $fillable = [
        'name',
        'num_code',
        'alpha_2_code',
        'alpha_3_code',
        'nationality'
    ];
}
