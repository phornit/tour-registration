<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'tbl_city';
    protected $fillable = [
        'post_code',
        'title_kh',
        'title_en',
        'is_active'
    ];
}
