<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class Tourists extends Model
{
    protected $table = 'tbl_tourists';
    protected $fillable = [
        'agency_id',
        'package_id',
        'passport',
        'first_name',
        'last_name',
        'gender',
        'dob',
        'phone',
        'email',
        'nationality',
        'country_id',
        'photo',
        'company',
        'is_tour_leader',
        'travel_from',
        'title',
        'vaccine_no',
        'vaccine_name',
        'number_dose',
        'last_dose',
        'vaccine_document'
    ];
}
