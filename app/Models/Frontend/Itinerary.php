<?php

namespace App\Models\Frontend;

use App\Libraries\Backend\CoreBackend;
use App\Models\Backend\City;
use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{
    protected $table = 'tbl_itinerary';
    protected $fillable = [
        'package_id',
        'days',
        'date_start',
        'time_start',
        'title',
        'description'
    ];

    public static function getItenerary($package_id){
        $Itinerary = Itinerary::where('package_id', $package_id)->orderBy('id','asc')->get();
        $data = $Itinerary->map(function($page){
            return [
                'id' => $page->id,
                'package_id' => $page->package_id,
                'days' => $page->days,
                'date_start' => $page->date_start,
                'time_start' => $page->time_start,
                'title' => $page->title,
                'description' => $page->description,
                'activities' => ItineraryActivity::getItineraryActivity($page->id)
            ];
        });

        return $data;
    }

    public static function getItineraryBroupByDays($package_id){
        $Itinerary = Itinerary::select('package_id','days_id')
                    ->where('package_id', $package_id)
                    ->groupBy('package_id', 'days_id')
                    ->get();
        $data = $Itinerary->map(function($page){
                return [
                    'days_id' => $page->days_id,
                    'package_id' => $page->package_id,
                    'days' => ItineraryDay::where('id', $page->days_id)->pluck('days')->first(),
                    'itinerary' => self::getIteneraryByDays($page->package_id, $page->days_id)
                ];
        });
        return $data;
    }

    public static function getIteneraryByDays($package_id, $days_id){
        $Itinerary = Itinerary::where('package_id', $package_id)->orderBy('id','asc')->get();
        $data = $Itinerary->map(function($page){
            return [
                'id' => $page->id,
                'package_id' => $page->package_id,
                'location_id' => $page->location_id,
                'location_name' => City::where('id', $page->location_id)->pluck('title_en')->first(),
                'title' => $page->title,
                'description' => $page->description,
                'dailyItenerary' => ItineraryActivity::getItineraryActivity($page->id),
                'safelocation' => self::safe_location($page->id)
            ];
        });

        return $data;
    }

    public static function safe_location($itinerary_id){
        $safelocation = SafeLocation::where('itinerary_id',$itinerary_id)->get();
        $data = $safelocation->map(function($page){
            return [
                'location_id' => $page->location_id,
                'package_id' => $page->package_id,
                'itinerary_id' => $page->itinerary_id,
                'safe_cate_id' => $page->safe_cate_id,
                'safe_cate_name' => $page->safe_cate_name,
                'safe_loc_id' => $page->safe_loc_id,
                'safe_loc_name' => $page->safe_loc_name,
                'safe_cate' => $page->safe_cate_id.','.$page->safe_cate_name,
                'safe_loc' => $page->safe_loc_id.','.$page->safe_loc_name,
                'safe_location' => CoreBackend::get_safe_location($page->safe_cate_id, $page->location_id)->data
            ];
        });
        return $data;
    }
}
