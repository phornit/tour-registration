<?php

namespace App\Models\Frontend;

use App\Libraries\Backend\CoreBackend;
use Illuminate\Database\Eloquent\Model;

class ItineraryActivity extends Model
{
    protected $table = "tbl_itinerary_activity";
    protected $fillable = [
        'package_id',
        'itinerary_id',
        'location_id',
        'safe_loc_id',
        'safe_cate_id',
        'safe_cate_name',
        'safe_loc_name',
        'time',
        'activity',
        'detail',
        'created_at',
        'updated_at',
    ];

    public static function getItineraryActivity($itinerary_id){
        $data = ItineraryActivity::where('itinerary_id', $itinerary_id)->get();
        $data = $data->map(function($page){
            return [
                'time' => $page->time,
                'activity' => $page->activity,
                'detail' => $page->detail,
                'location_id' => $page->location_id,
                'package_id' => $page->package_id,
                'itinerary_id' => $page->itinerary_id,
                'safe_cate_id' => $page->safe_cate_id,
                'safe_cate_name' => $page->safe_cate_name,
                'safe_loc_id' => $page->safe_loc_id,
                'safe_loc_name' => $page->safe_loc_name,
                'safe_cate' => $page->safe_cate_id.','.$page->safe_cate_name,
                'safe_loc' => $page->safe_loc_id.','.$page->safe_loc_name,
                'safe_location' => CoreBackend::get_safe_location($page->safe_cate_id, $page->location_id)->data
            ];
        });
        return $data;
    }
}
