<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class TourPackageTrackingPhoto extends Model
{
    protected $table = 'tbl_tour_package_tracking_photo';
    protected $fillable = [
        'id',
        'tracking_id',
        'photo'
    ];
}
