<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class TourPackageTrackingTourist extends Model
{
    protected $table = 'tbl_tour_package_tracking_tourist';
    protected $fillable = [
        'id',
        'tracking_id',
        'tourist_id',
        'tourist_name'
    ];
}
