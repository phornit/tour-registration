<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class TourPackage extends Model
{
    protected $table = 'tbl_tour_package';
    protected $fillable = [
        'agency_id',
        'leader_id',
        'title',
        'arrival_date',
        'departure_date',
        'num_day_tour',
        'meal',
        'transport',
        'activity',
        'description',
        'reason',
        'market',
        'hotel_qarantine',
        'insurance_company',
        'price',
        'is_approved',
        'is_register',
    ];

    public static function countPackageByagency($agencyId){
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');

        if ($fromDate != ''){
            $tourPackage = TourPackage::where('agency_id', $agencyId)->whereBetween('arrival_date',[$fromDate, $toDate])->pluck('id');
            $TotalPackage = TourPackage::whereIn('id',$tourPackage)->selectRaw('count(*) as TotalNumTP')->first();
        }else{
            $tourPackage = TourPackage::where('agency_id',$agencyId)->pluck('id');
            $TotalPackage = TourPackage::whereIn('id',$tourPackage)->selectRaw('count(*) as TotalNumTP')->first();
        }
        return $TotalPackage;
    }
    
    public static function countTouristByAgency($agencyId){
        $fromDate = \Request::get('fromdate');
        $toDate = \Request::get('todate');

        if ($fromDate != ''){
            $tourPackage = TourPackage::where('agency_id', $agencyId)->whereBetween('arrival_date',[$fromDate, $toDate])->pluck('id');
            $TotalTourists = Tourists::whereIn('package_id', $tourPackage)->selectRaw('count(*) as TotalTourists')->first();

        }else{
            $tourPackage = TourPackage::where('agency_id',$agencyId)->pluck('id');
            $TotalTourists = Tourists::whereIn('package_id', $tourPackage)->selectRaw('count(*) as TotalTourists')->first();
        }
        return $TotalTourists;
    }

    public static function TourPackageRegisterState($id){
        $current = date('Y-m-d H:i:s');
        $tourPackage = TourPackage::where('id',$id)->first();
        if($tourPackage){
            if(($current >= $tourPackage->arrival_date && $current <= $tourPackage->departure_date)){
                return 'ACTIVE';
            }else{
                return 'COMING';
            }
        }
    }

    public static function TourPackageNotRegisterState($id){
        $current = date('Y-m-d H:i:s');
        $tourPackage = TourPackage::where('id',$id)->first();
        if($tourPackage){
            if(($current >= $tourPackage->arrival_date)){
                return 'EXPIRED';
            }else if($tourPackage->is_register == 1){
                return 'PENDING';
            }else{
                return 'NEW';
            }
        }
    }

    public static function TourPackageAllState($id){
        $current = date('Y-m-d H:i:s');
        $tourPackage = TourPackage::where('id',$id)->first();
        if($tourPackage){
            if(($current >= $tourPackage->arrival_date && $current <= $tourPackage->departure_date)){
                return 'ACTIVE';
            }else if(($current <= $tourPackage->arrival_date && $tourPackage->is_register == 1)){
                return 'COMING';
            }else if(($current >= $tourPackage->arrival_date)){
                return 'EXPIRED';
            }else if($tourPackage->is_register == 1){
                return 'PENDING';
            }else{
                return 'NEW';
            }
        }
    }

}
