<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class ItineraryDay extends Model
{
    protected $table = "tbl_itinerary_day";
    protected $fillable = [
        'days',
        'created_at',
        'updated_at',
    ];
}
