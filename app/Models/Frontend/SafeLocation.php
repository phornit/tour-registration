<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class SafeLocation extends Model
{
    protected $table = 'tbl_safe_location';
    protected $fillable = [
        'id',
        'location_id',
        'package_id',
        'itinerary_id',
        'safe_cate_id',
        'safe_cate_name',
        'safe_loc_id',
        'safe_loc_name'
    ];
}
