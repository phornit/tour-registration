<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "tbl_menu";
    protected $fillable = [
        'parent_id',
        'parent_sub_id',
        'name',
        'alias',
        'type',
        'link_href',
        'link_cat_id',
        'link_catsub_id',
        'link_cont_id',
        'link_mod_id',
        'target',
        'ordering',
        'state',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'trashed',
        'trashed_by',
        'trashed_at'
    ];
}
