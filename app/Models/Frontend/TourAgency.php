<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class TourAgency extends Model
{
    protected $table = 'tbl_tour_agency';
    protected $fillable = [
        'id',
        'name',
        'description',
        'email',
        'phone',
        'address',
        'country_id',
        'logo'
    ];
}
