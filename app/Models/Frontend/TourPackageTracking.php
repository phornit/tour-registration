<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class TourPackageTracking extends Model
{
    protected $table = 'tbl_tour_package_tracking';
    protected $fillable = [
        'id',
        'agency_id',
        'package_id',
        'message',
        'state'
    ];
}
