<?php


namespace App\Modules\Frontend;

use App\Libraries\Backend\CoreConfig;
use App\Libraries\Backend\CoreSetting;
use App\Libraries\Backend\CoreTranslation;
use App\Models\Frontend\Menu;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class FrontEndMenu
{
    static function sub_nav_menu_id($list_nav_menu, $menu_id)
    {
        $parent = '';
        foreach ($list_nav_menu as $val)
        {
            if ($val->parent_id == $menu_id){
                $parent = $val->parent_id;
            }
        }
        return $parent;
    }

    static function sub_nav_menu_for_id($list_nav_menu, $menu_id)
    {
        $parentsub = '';
        foreach ($list_nav_menu as $val)
        {
            if ($val->parent_sub_id == $menu_id){
                $parentsub = $val->parent_sub_id;
            }
        }
        return $parentsub;
    }

    static function getNavMenu(){
        $menu = Menu::where('state',1)
            ->where('trashed',0)
            ->orderBy('ordering', 'ASC')
            ->get();

        return $menu;
    }

    static function build_frontend_menu()
    {
        $locale = (Config::get('app.locale') != Config::get('app.fallback_locale')) ? Config::get('app.locale').'/' : '';
        $list_nav_menu = self::getNavMenu();

        $html_nav_menu = '';
        $html_mob_menu = '';

        $html_nav_menu = "<ul class=\"nav\">";
        foreach ($list_nav_menu as $value)
        {
            if ($value['parent_id'] == 0)
            {
                $submenu_id  = self::sub_nav_menu_id($list_nav_menu, $value['id']);

                // parent link
                $nav_type = $value['type'];

                switch ($nav_type)
                {
                    case 1:
                        $nav_link = url($locale);
                        break;
                    case 2:
                        if (!empty($value['link_href'])) {

                            if (preg_match('#' . \Request::url() . '#', $value['link_href']))
                                $nav_link = $value['link_href'];
                            else {
                                if (preg_match('#http://#', $value['link_href']) || preg_match('#https://#', $value['link_href']))
                                    $nav_link = $value['link_href'];
                                else {
                                    $nav_link = url($locale.$value['link_href']);
                                }
                            }
                        }
                        break;
                    case 3:
                        $nav_link = CoreSetting::ContSEOURL($value['link_cont_id'], CoreSetting::getContSLUG($value['link_cont_id']));
                        break;
                    case 4:
                        $nav_link = CoreSetting::ContCateSEOURL($value['link_cat_id'], CoreSetting::getContCateSLUG($value['link_cat_id']));
                        //dd($nav_link);
                        break;
                    case 5:
                    default:
                        $nav_link = CoreSetting::ModSEOURL($value['link_mod_id'], CoreSetting::getModSLUG($value['link_mod_id']));
                        break;
                    case 6:
                        //dd("Working");
                        $nav_link = CoreSetting::ContCateparentSEOURL($value['link_catsub_id'], CoreSetting::getContCateParentSLUG($value['link_catsub_id']));

                        //dd($nav_link);
                        break;
                }

                //get data translate
                $current_lang_id = coreTranslation::getLanguageID(Config::get('app.locale'));
                $table = 'tbl_menu';
                $transFields = CoreConfig::main_menu_trans_fields();
                foreach ($transFields AS $transField)
                {
                    $transField = trim($transField);
                    $row_trans = coreTranslation::getTranslateData($current_lang_id, $table, $value['id'], 1);
                    if ($row_trans)
                    {
                        foreach ($row_trans as $val_trans)
                        {
                            if (!empty($val_trans->value))
                            {
                                $field = $val_trans->ref_field;
                                $value[$field]  = $val_trans->value;
                            }
                        }
                    }
                }

                // build menu
                if ($submenu_id === "")
                {
                    //menu no sub
                    $active_nav = (URL::current() == $nav_link) ? 'active' : '';
                    $html_nav_menu .= '<li><a href="'.$nav_link.'">'.$value['name'].'</a></li>';
                }
                else
                {
                    //dd($submenu_id);
                    //build sub and active parent
                    $active_navsub_menu = self::getMenuSubActive($list_nav_menu, $value['id']);
                    //dd($active_navsub_menu);

                    $html_nav_menu .= '<li class="has-t-submenu"><a href="#!">'.$value['name'].'</a><ul class="submenu">';


                    //do sub page
                    $html_nav_menu .= $active_navsub_menu['html_sub'];


                    $html_nav_menu.= '</ul></li>';

                }
                //dd($submenu_id);
            }
        }
        $html_nav_menu .="</ul>";
        return (object) array('frontend_menu' => $html_nav_menu, 'mobi_nav' => $html_mob_menu);
    }

    static function getMenuSubActive($list_nav_menu, $id)
    {
        $html_nav_menu = "";

        foreach ($list_nav_menu as $val_sub)
        {
            if ($val_sub->parent_id == $id)
            {
                $submenu_sub_id  = self::sub_nav_menu_for_id($list_nav_menu, $val_sub->id);
                // parent link
                $sub_type = $val_sub->type;
                switch ($sub_type)
                {
                    case 1:
                        $sub_link = url($locale);
                        break;
                    case 2:
                        if (!empty($val_sub->link_href)) {
                            if (preg_match('#' . \Request::url() . '#', $val_sub->link_href))
                                $sub_link = $val_sub->link_href;
                            else {
                                if (preg_match('#http://#', $val_sub->link_href) || preg_match('#https://#', $val_sub->link_href))
                                    $sub_link = $val_sub->link_href;
                                else {
                                    $sub_link = url($val_sub->link_href);
                                }
                            }
                        }
                        break;
                    case 3:
                        $sub_link = CoreSetting::ContSEOURL($val_sub->link_cont_id, CoreSetting::getContSLUG($val_sub->link_cont_id));
                        break;
                    case 4:
                        $sub_link = CoreSetting::ContCateSEOURL($val_sub->link_cat_id, CoreSetting::getContCateSLUG($val_sub->link_cat_id));
                        break;
                    case 5:
                    default:
                        $sub_link = CoreSetting::ModSEOURL($val_sub->link_mod_id, CoreSetting::getModSLUG($val_sub->link_mod_id));
                        break;
                }

                //get data translate
                $current_lang_id = CoreTranslation::getLanguageID(Config::get('app.locale'));
                $table = 'tbl_menu';
                $transFields = CoreConfig::main_menu_trans_fields();
                foreach ($transFields AS $transField)
                {
                    $transField = trim($transField);
                    $row_trans = CoreTranslation::getTranslateData($current_lang_id, $table, $val_sub->id, 1);
                    if ($row_trans)
                    {
                        foreach ($row_trans as $val_trans)
                        {
                            if (!empty($val_trans->value))
                            {
                                $field = $val_trans->ref_field;
                                $val_sub->$field = $val_trans->value;
                            }
                        }
                    }
                }

                //self active
                $active_sub = (URL::current() == $sub_link) ? 'active' : '';
                $active_parent[] = (URL::current() == $sub_link) ? 'active' : '';

                if($submenu_sub_id === "" AND $val_sub->parent_sub_id ==0)
                {
                    //sub html
                    $html_nav_menu.= '<li class="dropdown-item '.$active_sub.'"><a class="nav-link" href="'.$sub_link.'">'.$val_sub->name.'</a></li>';
                }
                elseif($val_sub->parent_sub_id ==0)
                {

                    $active_navsub_menu = self::getMenuSubActivefor($list_nav_menu, $val_sub->id);
                    //dd($active_navsub_menu['active_parent1']);
                    $html_nav_menu .='<!-- Blog - Single items -->
                    <li class="dropdown-item hs-has-sub-menu '.(isset($active_navsub_menu['active_parent1']) ?$active_navsub_menu['active_parent1']:'').'">
                      <a id="nav-link--frontend--blog--single-item" class="nav-link" href="#!" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--frontend--blog--single-item">'.$val_sub->name.'</a>
                      <!-- Submenu (level 2) -->
                      <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-brd-top g-brd-primary g-brd-top-2 g-min-width-220 g-mt-minus-2" id="nav-submenu--frontend--blog--single-item" aria-labelledby="nav-link--frontend--blog--single-item">';
                    //do sub page
                    $html_nav_menu .= $active_navsub_menu['html_sub1'];
                    //dd($html_nav_menu);
                    $html_nav_menu.= '</ul></li>';
                    //dd($html_nav_menu);
                }
            }
        }
        //dd($html_nav_menu);

        return ['active_parent' => in_array('active', $active_parent) ? 'active' : '', 'html_sub' => $html_nav_menu];
    }

    static function getMenuSubActivefor($list_nav_menu, $id)
    {
        $html_nav_menu = "";
        $active_parent1=[];

        foreach ($list_nav_menu as $val_sub)
        {
            if ($val_sub->parent_sub_id ==$id)
            {

                $submenu_sub_id  = self::sub_nav_menu_for_id($list_nav_menu, $val_sub->id);
                // parent link
                $sub_type = $val_sub->type;
                switch ($sub_type)
                {
                    case 1:
                        $sub_link = url($locale);
                        break;
                    case 2:
                        if (!empty($val_sub->link_href)) {
                            if (preg_match('#' . url() . '#', $val_sub->link_href))
                                $sub_link = $val_sub->link_href;
                            else {
                                if (preg_match('#http://#', $val_sub->link_href) || preg_match('#https://#', $val_sub->link_href))
                                    $sub_link = $val_sub->link_href;
                                else {
                                    $sub_link = url($val_sub->link_href);
                                }
                            }
                        }
                        break;
                    case 3:
                        $sub_link = CoreSetting::ContSEOURL($val_sub->link_cont_id, CoreSetting::getContSLUG($val_sub->link_cont_id));
                        break;
                    case 4:
                        $sub_link = CoreSetting::ContCateSEOURL($val_sub->link_cat_id, CoreSetting::getContCateSLUG($val_sub->link_cat_id));
                        break;
                    case 5:
                    default:
                        $sub_link = CoreSetting::ModSEOURL($val_sub->link_mod_id, CoreSetting::getModSLUG($val_sub->link_mod_id));
                        break;
                }

                //get data translate
                $current_lang_id = CoreTranslation::getLanguageID(Config::get('app.locale'));
                $table = 'tbl_menu';
                $transFields = CoreConfig::main_menu_trans_fields();
                foreach ($transFields AS $transField)
                {
                    $transField = trim($transField);
                    $row_trans = CoreTranslation::getTranslateData($current_lang_id, $table, $val_sub->id, 1);
                    if ($row_trans)
                    {
                        foreach ($row_trans as $val_trans)
                        {
                            if (!empty($val_trans->value))
                            {
                                $field = $val_trans->ref_field;
                                $val_sub->$field = $val_trans->value;
                            }
                        }
                    }
                }

                //self active
                $active_sub1 = (URL::current() == $sub_link) ? 'active1' : '';
                $active_parent1[] = (URL::current() == $sub_link) ? 'active1' : '';
                //sub html
                $html_nav_menu.= '<li class="dropdown-item '.$active_sub1.'"><a class="nav-link" href="'.$sub_link.'">'.$val_sub->name.'</a></li>';

            }
        }
        //dd($html_nav_menu);

        return ['active_parent1' => in_array('active1', $active_parent1) ? 'active1' : '', 'html_sub1' => $html_nav_menu];
    }
}
