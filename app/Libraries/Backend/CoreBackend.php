<?php

namespace App\Libraries\Backend;

use App\Http\Controllers\Controller;

use App\Models\Auth\UserProfile;
use App\User;


class CoreBackend extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    static function getCurrentSecUserId(){
        $data = auth()->user()->id;
        return $data;
    }

    static function getCurrentSecUser(){
        $data = User::find(auth()->user()->id);
        return $data;
    }

    static function getCurrentUserRole(){
        $currentUserRole = User::where('id',auth()->user()->id)->pluck('user_role');
        return $currentUserRole;
    }

    static function getCurrentUserInfo(){
        $data = UserProfile::where('sec_user_id',auth()->user()->id)->first();
        return $data;
    }

    static function currentAgencyId(){
        return self::getCurrentUserInfo()['agency_id'];
    }

    static function get_safe_location($cat_id, $loc_id){
                
        $client = new \GuzzleHttp\Client();
        $url = "https://hotelsafety.visitcambodia.travel/api/hotelapi?cat_id=". $cat_id .'&pro_id='. $loc_id;

        $headers = [
            'X-API-KEY' => '1ccbc4c913bc4ce785a0a2de444aa0d6'
        ];

        $response = $client->request('GET', $url, [
            'headers' => $headers,
            'verify'  => false,
        ]);

        return json_decode($response->getBody());
    }

}
