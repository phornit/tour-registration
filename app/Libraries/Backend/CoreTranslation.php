<?php


namespace App\Libraries\Backend;


use Illuminate\Support\Facades\DB;

class CoreTranslation
{
    static function isLanguageActive ($id)
    {
        $query = DB::table('core_languages');
        $query->where('id', $id);
        $query->where('state', 1);
        $results = $query->get();
        if ($results)
        {
            return true;
        }
    }

    static function getFlagViaID ($id)
    {
        $query = DB::table('core_languages');
        $query->where('id', $id);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->image;
        }
    }

    static function getLanguageID ($locale)
    {
        $query = DB::table('core_languages');
        $query->where('localization', $locale);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->id;
        }
    }

    static function getTranslateData ($lang_id, $ref_table, $ref_id, $state = NULL)
    {
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->where('ref_id', $ref_id);

        if (!is_null($state))
        {
            $query->where('state', $state);
        }

        $results = $query->get();

        if ($results)
        {
            return $results;
        }
    }

    static function getTranslateField ($lang_id, $ref_table, $ref_id, $ref_field)
    {
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->where('ref_id', $ref_id);
        $query->where('ref_field', $ref_field);

        $results = $query->get();

        if ($results)
        {
            return $results[0];
        }
    }

    static function isExistedTranslate($lang_id, $ref_table, $ref_id, $ref_field)
    {
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->where('ref_id', $ref_id);
        $query->where('ref_field', $ref_field);

        $results = $query->get();

        if ($results)
        {
            return true;
        }
    }

    static function createTranslate ($lang_id, $ref_table, $ref_id, $ref_field, $value, $state)
    {
        DB::table('tbl_translation')->insertGetId(
            [
                'lang_id' 		=> $lang_id,
                'ref_table'		=> $ref_table,
                'ref_id'		=> $ref_id,
                'ref_field'		=> $ref_field,
                'value'			=> $value,
                'state'			=> $state,
                'updated_by'	=> Auth::id(),
                'updated_at'	=> date('created_at')
            ]
        );

        return $ref_id;
    }

    static function updateTranslate ($lang_id, $ref_table, $ref_id, $ref_field, $value, $state)
    {
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->where('ref_id', $ref_id);
        $query->where('ref_field', $ref_field);
        $query->update(
            [
                'value'			=> $value,
                'state'			=> $state,
                'updated_by'	=> Auth::id(),
                'updated_at'	=> date('created_at')
            ]
        );

        return $ref_id;
    }

    static function setTransState($lang_id, $ref_table, $ref_id, $state)
    {
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->where('ref_id', $ref_id);
        $query->update(
            [
                'state'			=> $state
            ]
        );
        return true;
    }

    static function setSelectedTransState($lang_id, $ref_table, $ref_ids, $state)
    {
        $arr_ids = explode(',', $ref_ids);
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->whereIn('ref_id', $arr_ids);
        $query->update(
            [
                'state'			=> $state
            ]
        );
        return true;
    }

    static function goDeleteTransRec($lang_id, $ref_table, $ref_ids)
    {
        $arr_ids = explode(',', $ref_ids);
        $query = DB::table('tbl_translation');
        $query->where('lang_id', $lang_id);
        $query->where('ref_table', $ref_table);
        $query->whereIn('ref_id', $arr_ids);
        $query->delete();

        return true;
    }
}
