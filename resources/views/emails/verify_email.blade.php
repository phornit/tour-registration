<!DOCTYPE html>
<html>
<head>
    <title>VACCINATION TOUR PACKAGE</title>
</head>
<body>
<h3>Welcome {{$data['name']}},</h3>
<br/>
<p>Thank you for registering with the website <a href="http://tpr.visitcambodia.travel">VACCINATION TOUR PACKAGE</a></p>
<p>Please click this link <a href="http://tpr.visitcambodia.travel/verify-account/{{Crypt::encrypt($data['id'])}}">Verify Account</a> to access your account.</p>
<br/>
Thanks!<br/>
Email : info@tpr.visitcambodia.travel<br/>
Website :<a href="www.tpr.visitcambodia.travel">www.tpr.visitcambodia.travel </a>
</body>
</html>
