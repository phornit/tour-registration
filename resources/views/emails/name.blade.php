<!DOCTYPE html>
<html>
<head>
    <title>Cambodia 4.0 Center</title>
</head>
<body>
<h1>{{$name}}</h1>
<p>សូមអរគុណ សម្រាប់ការចុះឈ្មោះ</p> <br/>
<p>ពាក្យសុំចូលជាសមាជិក {{$name}} ត្រូវបានអនុញ្ញាតិ។</p><br/><br/>
សូមអរគុណ!<br/>
លេខទូរស័ព្ទ : +855 12 628 665<br/>
អ៊ីមែល : info@cambodia4point0.org<br/>
គេហទំព័រ :<a href="www.cambodia4point0.org">​​www.cambodia4point0.org </a>
</body>
</html>
