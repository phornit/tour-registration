
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="user" content="{{ optional(Auth::user())->id }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>VACCINATION TOUR PACKAGE</title>
    <link rel="shortcut icon" href="/img/logo_mot.png">
    <link rel="stylesheet" href="/css/app.css?v=1.0.15">
    <link rel="stylesheet" href="/css/responsive.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Battambang:wght@400;700&display=swap');
        *{
            font-family: 'Battambang', "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: 0.96rem;
            font-weight: 400;
        }
        .navbar-dark .navbar-nav .nav-link{
            color: #fff;
        }
        .navbar-collapse{
            text-align: center;
        }
        .navbar-collapse {
            flex-grow: 0;
        }
        .router-link-exact-active {
            background-color: #3c8dbc;
            color: #fff !important;
            border-radius: 10px;
            padding-bottom: 30px !important;
            border-bottom: 2px solid #f4f6f9;
        }
        .text-sm {
            font-size: 0.65rem !important;
        }
        ::-webkit-input-placeholder { /* Edge */
            color: red;
        }

        :-ms-input-placeholder { /* Internet Explorer */
            color: red;
        }
        ::placeholder {
            color: red;
        }
        .modal-dialog {
            max-width: 800px !important;
        }
        .modal-body {
            padding: 1.5rem !important;
        }
        .action-title {
            width: 185px;
            text-align: right;
        }
        .padding{
            padding-top: 1px;
            padding-bottom: 1px;
        }
    </style>
</head>

<body class="hold-transition sidebar-collapse layout-top-nav text-sm">
<div class="wrapper" id="app">
    <router-view></router-view>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script src="/js/app.js?v=1.0.23"></script>
</body>
</html>
