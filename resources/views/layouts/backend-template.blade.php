<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="user" content="{{ optional(Auth::user())->id }}">
  <title>VACCINATION TOUR PACKAGE | Content Management System (CMS)</title>

  <link rel="shortcut icon" href="/images/logo_small.png">

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="/css/app.css?v=1.1.30">

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />

  <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
  <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <style lang="scss">
      a {
          color: rgb(0, 0, 0);
      }
      @font-face {
          font-family: "Odor Mean Chey";

          src: url('/fonts/khmer-font/KhmerOSContent-Bold.ttf');
          src: url('/fonts/khmer-font/KhmerOSContent-Regular.ttf');
          src: url('/fonts/khmer-font/OdorMeanChey.ttf');
          font-weight: 400;
          font-style: normal;
      }
      .router-link-exact-active {
            background-color: #343a40;
            color: #fff !important;
        }
  </style>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

<router-view></router-view>
</div>
<!-- ./wrapper -->

@auth
<script>
    window.user = @json(auth()->user())
</script>
@endauth
<script src="/js/app.js?v=1.1.34"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
</body>
</html>
