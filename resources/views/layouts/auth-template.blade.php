
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>VACCINATION TOUR PACKAGE</title>
    <!-- Tell the browser to be responsive to screen width -->
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="user" content="{{ optional(Auth::user())->id }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="shortcut icon" href="/img/logo_mot.png">
    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }
        @font-face {
            font-family: "Odor Mean Chey";
            src: url('/fonts/khmer-font/OdorMeanChey.ttf');
            font-weight: 400;
            font-style: normal;
        }
        body {
            font: 100 0.8em/0.8em "Odor Mean Chey", sans-serif;
            font-weight: normal;
            height: 100%;
            overflow-x: hidden;
            letter-spacing: 0.2px;
            line-height: 20px;
        }
        .login-page,
        .register-page {
            -ms-flex-align: center;
            align-items: center;
            background: #e9ecef;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 95vh;
            -ms-flex-pack: center;
            justify-content: center;
        }
        .rigister-title{
            font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: 1rem;
            font-weight: 400;
            background-color: #3490dc;
            text-align: left;
            padding: 10px;
            margin-bottom: 5px;
            border-bottom: 2px solid #45657f;
            text-transform: capitalize;
            color: white;
        }
    </style>
</head>
<body class="hold-transition register-page text-sm">
<div id="app">
    <router-view></router-view>
</div>

<!-- /.register-box -->

<!-- jQuery -->
<script src="/js/app.js?v=1.0.0"></script>
</body>
</html>
