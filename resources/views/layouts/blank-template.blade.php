
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="user" content="{{ optional(Auth::user())->id }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tour Package Registration</title>
    <link rel="shortcut icon" href="/img/logo_mot.png">
    <link rel="stylesheet" href="/css/app.css?v=1.0.11">
    <link rel="stylesheet" href="/css/responsive.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        .navbar-dark .navbar-nav .nav-link{
            color: #fff;
        }
        .navbar-collapse{
            text-align: center;
        }
        .navbar-collapse {
            flex-grow: 0;
        }
        .text-sm .main-header .nav-link, .main-header.text-sm .nav-link {
            height: 1.93725rem;
            padding: 1.35rem 1rem;
        }
    </style>
</head>
<body style="background-color: white;">
<div class="wrapper" id="app">
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <!-- /.navbar -->
    <router-view></router-view>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="/js/app.js?v=1.0.8"></script>
</body>
</html>
