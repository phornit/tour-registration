
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cambodia 4.0 Center | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/app.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />

    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }
        @font-face {
            font-family: "Odor Mean Chey";
            src: url('/fonts/khmer-font/OdorMeanChey.ttf');
            font-weight: 400;
            font-style: normal;
        }
        body {
            font: 100 0.8em/0.8em "Odor Mean Chey", sans-serif;
            font-weight: normal;
            height: 100%;
            overflow-x: hidden;
            letter-spacing: 0.2px;
            line-height: 20px;
        }
        .login-page,
        .register-page {
            -ms-flex-align: center;
            align-items: center;
            background: #0c233a;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 70vh;
            -ms-flex-pack: center;
            justify-content: center;
        }
        .login-box, .register-box {
            width: 460px;
        }
        .invalid-feedback {
            color: #a93c3c;
            background-color: #eca8a885;
            border-color: #efabab;
            font-size: 12px;
            font-weight: normal;
        }
        ...
    </style>

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img src="/img/logo_mot.png" alt="Tour Package Registration Logo" style="width: 200px;">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <h3><p class="login-box-msg"> Reset Password</p></h3>

            <form action="{{ url('/reset_password_with_token') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="email"​id="email" class="form-control" placeholder="E-Mail Address" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-5">
                        <a href="/login" class="btn btn-info btn-block">Sign In </a>
                    </div>
                    <!-- /.col -->
                    <div class="col-7">
                        <button type="submit" class="btn btn-primary btn-block">Send Password Reset Link​</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br/>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            @error('email')
            <span class="alert invalid-feedback d-block" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>
        <!-- /.login-card-body -->




    </div>
</div>

@auth
    <script>
        window.user = @json(auth()->user())
    </script>
@endauth
<script src="/js/app.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>

</body>
</html>



{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Reset Password') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    @if (session('status'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ session('status') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    <form method="POST" action="{{ url('/reset_password_with_token') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('emails') is-invalid @enderror" name="email" value="{{ old('emails') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('emails')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Send Password Reset Link') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}
