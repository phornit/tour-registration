
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cambodia 4.0 Center | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/app.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />

    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }
        @font-face {
            font-family: "Odor Mean Chey";
            src: url('/fonts/khmer-font/OdorMeanChey.ttf');
            font-weight: 400;
            font-style: normal;
        }
        body {
            font: 100 0.8em/0.8em "Odor Mean Chey", sans-serif;
            font-weight: normal;
            height: 100%;
            overflow-x: hidden;
            letter-spacing: 0.2px;
            line-height: 20px;
        }
        .login-page,
        .register-page {
            -ms-flex-align: center;
            align-items: center;
            background: #0c233a;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 70vh;
            -ms-flex-pack: center;
            justify-content: center;
        }
        ...
    </style>

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img src="/img/logo_mot.png" alt="Tour Package Registration Logo" style="width: 200px;">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <h3><p class="login-box-msg"> ភ្លេចពាក្យសម្ងាត់ </p></h3>

            <form action="{{ url('/password') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input type="email"​id="email" class="form-control" placeholder="អ៊ីម៉ែល (Email)" @error('emails') is-invalid @enderror" name="email" value="{{ old('emails') }}" required autocomplete="email" autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>

                    @error('emails')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>

                <div class="row">
                    <div class="col-8" style="max-width: 62.6666666667%;">
                        <p class="mb-0">
                            <a href="/login" class="text-center" style="font-size: 12px; color: #6d7a86;"> ចូលប្រព័ន្ធ </a> <br/>
                        </p>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block" style="width: 100px; font-size: 12px;">​ផ្ញើរទៅអ៊ីមែល​</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br/>
        </div>
        <!-- /.login-card-body -->


        @if(Session::has('message'))
            {!! Session::get('message') !!}
        @endif

    </div>
</div>

@auth
    <script>
        window.user = @json(auth()->user())
    </script>
@endauth
<script src="/js/app.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>

</body>
</html>
