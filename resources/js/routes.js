import register from './components/auth/register'
import login from './components/auth/login'

import app from './components/layouts/App.vue'
import backend from './components/layouts/Backend'

import home from './components/home.vue'
import Dashboard from './components/dashboard.vue'

import UserList from './components/auth/users/user-list.vue'
import UserAdd from './components/auth/users/user-add.vue'
import UserEdit from './components/auth/users/user-edit.vue'
import Profile from './components/auth/users/user-profile.vue'

import Audit from './components/auth/Audit/audit-list'

import backendMenu from './components/backend/adminMenu/admin-menu'

import MenuList from './components/backend/menu/menu-list.vue'
import MenuAdd from './components/backend/menu/menu-add.vue'
import MenuEdit from './components/backend/menu/menu-edit.vue'

import RoleList from './components/auth/roles/role-list.vue'

import file_upload from './components/corebackend/file-upload.vue'
import resizeImagePost from './components/corebackend/resizeImagePost.vue'

//Tour Package Register
import TourPackageList from './components/TourPackage/tour-package-list'
import EditTourPackage from './components/TourPackage/edit-tour-package'
import AddTourPackage from './components/TourPackage/add-tour-package'

//Tour Agency Backend
import TourAgencyInfo from './components/Backend/Tour/TourAgencyInfo'
import TourPackageInfo from './components/Backend/Tour/TourPackageInfo'
import TourPackageDetail from './components/Backend/Tour/TourPackageDetail'

import agencyRegister from './components/TourPackage/agency-register'
import Setting from './components/TourPackage/Setting'
import TouristsList from './components/TourPackage/tourists-list'
import TouristsListByPackage from './components/TourPackage/tourists-list'
import Itinerary from './components/TourPackage/itinerary'

//Team leader
import TeamLeader from './components/TourPackage/team-leader-list'
import TeamLeaderProfile from './components/TourPackage/team-leader-profile'

//Notification
import Notification from './components/TourPackage/notification'

//Report
import TourPackageReportFrontend from './components/TourPackage/tour-package-frontend-report'

//Report Backend
import TourAgencyReport from './components/Reports/tourAgencyReport'
import TourPackageReport from './components/Reports/tourPakcageReport'
import TourPackageDetailReport from './components/Reports/tourPackageDetailReport'
import TouristBackendReport from './components/Reports/touristsBackendReport'
import BackendReport from './components/Reports/backend-report'

export const routes = [{
        name: 'login',
        path: '/',
        component: login
    },
    {
        name: 'register',
        path: '/register',
        component: register
    },
    {
        name: 'backend',
        path: '/backend',
        component: backend,
        children: [
            { name: 'dashboard', path: '/backend/dashboard', component: Dashboard },
            { name: 'RoleList', path: '/backend/role-list', component: RoleList },
            { name: 'UserList', path: '/backend/user-List', component: UserList },
            { name: 'UserAdd', path: '/backend/user-add', component: UserAdd },
            { name: 'UserEdit', path: '/backend/user-edit/:userid', component: UserEdit },
            { name: 'Profile', path: '/backend/profile', component: Profile },
            { name: 'BackendMenu', path: '/backend/backend-menu', component: backendMenu },
            { name: 'MenuList', path: '/backend/menu-list', component: MenuList },
            { name: 'MenuAdd', path: '/backend/menu-add', component: MenuAdd },
            { name: 'MenuEdit', path: '/backend/menu-edit', component: MenuEdit },
            { name: 'Audit', path: '/backend/audit', component: Audit },
            { name: 'agencyRegister', path: '/tour-agency-register', component: agencyRegister },
            { name: 'TourAgencyInfo', path: '/backend/agency-info', component: TourAgencyInfo },
            { name: 'TourPackageInfo', path: '/backend/tour-package', component: TourPackageInfo },
            { path: 'tour-package-detail/:package_id', name: 'TourPackageDetail', component: TourPackageDetail },
            { name: 'file_upload', path: '/file_upload', component: file_upload },
            { name: 'resizeImagePost', path: '/resizeImagePost', component: resizeImagePost },
            { name: 'TourAgencyReport', path: '/backend/tour-agency-backend-report', component: TourAgencyReport },
            { name: 'TourPackageReport', path: '/backend/tour-package-backend-report', component: TourPackageReport },
            { name: 'TouristBackendReport', path: '/backend/tourist-backend-report', component: TouristBackendReport },
            { name: 'TourPackageDetailReport', path: '/backend/tourist-backend-detail-report/:package_id', component: TourPackageDetailReport },
            { name: 'BackendReport', path: '/backend/backend-report', component: BackendReport },

        ]
    },
    {
        name: 'agencyProfile',
        path: '/app',
        component: app,
        children: [
            { name: 'home', path: '/home', component: home },
            { path: '/tour-package-list', name: 'TourPackageList', component: TourPackageList },
            { path: '/add-tour-package', name: 'AddTourPackage', component: AddTourPackage },
            { path: '/edit-tour-package/:package_id', name: 'EditTourPackage', component: EditTourPackage },
            { path: '/team-leader-list', name: 'TeamLeader', component: TeamLeader },
            { path: '/team-leader-profile/:team_leader_id', name: 'TeamLeaderProfile', component: TeamLeaderProfile },
            { path: '/setting', name: 'Setting', component: Setting },
            { path: '/tourists-list/:package_id', name: 'TouristsListByPackage', component: TouristsListByPackage },
            { path: '/itinerary-list/:package_id', name: 'Itinerary', component: Itinerary },
            { path: '/tourists-list', name: 'TouristsList', component: TouristsList },
            { path: '/tour-package-frontend-report/:package_id', name: 'TourPackageReportFrontend', component: TourPackageReportFrontend },
            { path: '/notification', name: 'Notification', component: Notification },
        ]
    }
];