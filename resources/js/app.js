/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Store from './Store';
window.Fire = new Vue();

// axios.defaults.baseURL = 'http://www.mdo-cambodia.org';

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['accept'] = 'application/json';
axios.defaults.headers.common['X-API-KEY'] = '1ccbc4c913bc4ce785a0a2de444aa0d6';

let access_token = Store.getters.getAccessToken;

if (access_token)
    axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;

window.Store = Store;

Vue.prototype.$accessToken = Store.getters.getAccessToken;
Vue.prototype.$currentUser = Store.getters.getCurrentUser;

import { Form, HasError, AlertError } from 'vform';
import moment from 'moment';

// vue router
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import { routes } from './routes';

import VueProgressBar from 'vue-progressbar';

Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
})

window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

Vue.component('pagination', require('laravel-vue-pagination'));

// Format datetime Ex : {{user.created_at | myDate}}
Vue.filter('myDate', function(created) {
    return moment(created).format('MMMM Do YYYY');
});

Vue.filter('myDOB', function(created) {
    return moment(created).format('DD-MM-YYYY');
});

Vue.filter('dateCreated', function(created) {
    return moment(created).format('DD-MM-YYYY hh:mm:ss');
});

Vue.filter('getDateCreated', function(created) {
    return moment(created).format('DD-MMM-YYYY hh:mm a');
});

// Get user login from frontend meta
Vue.prototype.$user = document.querySelector("meta[name='user']").getAttribute('content');

//progress bar
import ProgressBar from 'vuejs-progress-bar'
Vue.use(ProgressBar);

//Popup Form
import swal from 'sweetalert2';
window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.toast = toast;

window.Fire = new Vue();

import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';
import CxltToastr from 'cxlt-vue2-toastr';

var toastrConfigs = {
    position: 'top right',
    showDuration: 20
};
Vue.use(CxltToastr, toastrConfigs);

//Loading page
import VueLoading from 'vuejs-loading-plugin'

// using default options
Vue.use(VueLoading);

// overwrite defaults
Vue.use(VueLoading, {
    dark: true, // default false
    text: 'Ladataan', // default 'Loading'
    loading: true, // default false
    //   customLoader: myVueComponent, // replaces the spinner and text with your own
    background: 'rgb(255,255,255)', // set custom background
    classes: ['myclass'] // array, object or string
});

import VTooltip from 'v-tooltip'
Vue.use(VTooltip)

import excel from 'vue-excel-export'
Vue.use(excel)

import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use(CKEditor);

//then put this into your main.js file anywhere AFTER loading Vue
// import VueMeta from 'vue-meta'
// Vue.use(VueMeta, {
//     // optional pluginOptions
//     refreshOnceOnNavigation: true
// })


import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: 'history',
});

// Print
import VueHtmlToPaper from 'vue-html-to-paper';
const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ],
    styles: [
        "/css/print.css"
    ]
}

Vue.use(VueHtmlToPaper, options);

const app = new Vue({
    el: '#app',
    router,
    data: {
        search: ''
    },
    methods: {
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        }, 1000),

        printme() {
            window.print();
        }
    }
});